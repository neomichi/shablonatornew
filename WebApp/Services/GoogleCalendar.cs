﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Google;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Auth.OAuth2.Responses;
using Google.Apis.Calendar.v3;
using Google.Apis.Calendar.v3.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using Microsoft.AspNetCore.Http;
using Microsoft.DotNet.Cli.Utils.CommandParsing;
using Microsoft.Extensions.Options;
using WebApp.Code;
using WebApp.Models;
using WebApp.ViewModels.Google;


namespace WebApp.Services
{





    public class GoogleCalendar : IGoogleCalendar
    {
        #region setting
        private readonly GoogleOptions _options;
        private readonly IMyLogger _logger;
        public GoogleCalendar(IOptions<GoogleOptions> options, IMyLogger logger)
        {
            _options = options.Value;
            _logger = logger;
        }

        enum Action
        {
            Delete, AddOrUpdate
        }

        #endregion




        async Task Init(Action action, Event _event)
        {
            var ct = new CancellationTokenSource();
            //  ct.CancelAfter(3000);

            UserCredential credential = null;

        

            const string urlAuth =
                "https://accounts.google.com/o/oauth2/auth?" +
                "redirect_uri=http://localhost/google-auth&" +
                "response_type=code&" +
                "client_id=333937315318-fhpi4i6cp36vp43b7tvipaha7qb48j3r.apps.googleusercontent.com&" +
                "scope=https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile";
            
          



            //var flow = new GoogleWebAuthorizationBroker(new GoogleAuthorizationCodeFlow.Initializer
            //{
            //    ClientSecrets = new ClientSecrets
            //    {
            //        ClientId = _options.ClientId,
            //        ClientSecret = _options.ClientSecret,
            //    },
            //    Scopes = new[] {CalendarService.Scope.Calendar},
               
               
            //});

    
            var str =
                "https://accounts.google.com/o/oauth2/auth?" +
                "redirect_uri=http://localhost/admin/Test&" +
                "response_type=code&" +
                "client_id="+_options.ClientId+"&" +
                "scope=https://www.googleapis.com/auth/calendar";


            var httpClient = new HttpClient();
          



    

          

            try
            {

            }
            catch
            {
                _logger.Log("ошибка авторизации google");
                _logger.Log(string.Format("ClientId {0}", _options.ClientId));
                _logger.Log(string.Format("ClientSecret {0}", _options.ClientSecret));

                ct.Cancel();
            }
            var service = new CalendarService(new BaseClientService.Initializer()
            { 
                HttpClientInitializer = credential,
                ApplicationName = _options.GoogleApplicationName
            });

            var calendars = await service.CalendarList.List().ExecuteAsync();

            //показываем только наш календарь и его события
            var calendar = calendars.Items.SingleOrDefault(x => x.Summary.Eq(_options.CalendarName));

            if (calendar == null)
            {
                _logger.Log(String.Format("нет указанного календаря: {0}", _options.CalendarName));
                ct.Cancel();
                return;
            }

            var events = service.Events.List(calendar.Id).Execute().Items.ToList();


            switch (action)
            {

                case Action.Delete:
                    {
                        var eventExists = events.FirstOrDefault(x => Helper.EqualsToUniqueId(x.Id, _event.Id));
                        if (eventExists != null)
                        {
                            service.Events.Delete(calendar.Id, eventExists.Id).Execute();
                        }
                        break;
                    }
                case Action.AddOrUpdate:
                    {
                        try
                        {
                            var eventExists = events.FirstOrDefault(x => Helper.EqualsToUniqueId(x.Id, _event.Id));
                            if (eventExists != null)
                            {
                                var eventUpdate = service.Events.Get(calendar.Id, eventExists.Id).Execute();
                                eventUpdate.Start = _event.Start;
                                eventUpdate.End = _event.End;
                                eventUpdate.Description = _event.Description;
                                eventUpdate.Summary = _event.Summary;
                                eventUpdate.Location = _event.Location;
                                eventUpdate.ColorId = _event.ColorId;
                                service.Events.Update(eventUpdate, calendar.Id, eventUpdate.Id).Execute();

                            }
                            else
                            {
                                service.Events.Insert(_event, calendar.Id).Execute();
                            }
                        }
                        catch (GoogleApiException g)
                        {
                            throw new GoogleApiException(g.ServiceName, g.Message);
                        }
                        break;
                    }
            }



            ////тут можно указать правила отображения
            // request.SingleEvents = true;
            //request.TimeMin = DateTime.Now.AddMonths(-3);
            //request.TimeMax = DateTime.Now.AddMonths(3);
            //тут можно указать правила

            //var fetchResults = await request.ExecuteAsync();
            service.Dispose();
        }

        /// <summary>
        /// создаем событие  с уникальным Id от AuctionProfile
        /// </summary>
        /// <param name="auctionProfile"></param>
        /// <returns></returns>
        Event CreateEvent(AuctionProfile auctionProfile)
        {
            var _event = new Event
            {
                Start = new EventDateTime()
                {
                    DateTime = auctionProfile.AuctionDateStart,
                    TimeZone = "Europe/Moscow",
                },
                End = new EventDateTime()
                {
                    DateTime = auctionProfile.AuctionDateEnd,
                    TimeZone = "Europe/Moscow",
                },
                ColorId = "11",
                Kind = "calendar#events",
                Id = auctionProfile.AuctionProfileId.ToUniqueId(),
                Summary = string.Format("аукцион №{0}", auctionProfile.AuctionProfileId),
                Description = string.Format("аукцион №{0}", auctionProfile.AuctionProfileId)
            };
            return _event;
        }


        /// <summary>
        /// Добавить событие
        /// </summary>
        /// <param name="auctionProfile">аукцион</param>
        /// <returns></returns>
        public async Task AddOrUpdate(AuctionProfile auctionProfile)
        {
            await Init(Action.AddOrUpdate, CreateEvent(auctionProfile));

        }

        /// <summary>
        /// удалить событие
        /// </summary>
        /// <param name="auctionProfile">аукцион</param>
        /// <returns></returns>
        public async Task Remove(AuctionProfile auctionProfile)
        {
            await Init(Action.Delete, CreateEvent(auctionProfile));

        }
    }
}
