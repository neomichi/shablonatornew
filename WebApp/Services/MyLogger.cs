﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.PlatformAbstractions;

namespace WebApp.Services
{
    public class MyLogger : IMyLogger
    {
        private readonly IHostingEnvironment _appEnvironment;
        public MyLogger(IHostingEnvironment appEnvironment)
        {
            _appEnvironment = appEnvironment;
        }

        static readonly object Lock = new object();

        public void Log(string message)
        {
            Save($"{DateTime.Now:hh:mm:ss}: {message}");
        }     



        private void Save(string message)
        {
            var filename = "log.txt";
            var path = _appEnvironment.WebRootPath;
            var fullpath = Path.Combine(path, "log", string.Format("{0:dd-MM-yyyy}", DateTime.Now));



            if (!Directory.Exists(fullpath)) Directory.CreateDirectory(fullpath);
            var fullname = Path.Combine(fullpath, filename);
            
            var list = new List<string> { message };

            lock (Lock)
            {
                File.AppendAllLines(fullname, list, Encoding.UTF8);
            }


        }


    }
}
