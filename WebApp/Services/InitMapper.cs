﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using WebApp.Code;
using WebApp.Models;
using WebApp.ViewModels.Auction;
using WebApp.ViewModels.Profile;
using WebApp.ViewModels.User;
using WebApp.ViewModels.WaitingUser;

namespace WebApp.Services
{


    public class InitMapper
    {
        public InitMapper()
        {
            

            Mapper.CreateMap<Organization, AuctionView>()
                .ForMember(x => x.OrganizationName,
                o => o.ResolveUsing(x => x.Name));

            Mapper.CreateMap<AuctionProfile, AuctionView>().BothWays();
            Mapper.CreateMap<ApplicationUser, UserWithRole>()
                .ForMember(x => x.FullName,
                  z => z.ResolveUsing(x => string.IsNullOrWhiteSpace(x.FullName) ? "не указан" : x.FullName))
                .ForMember(x => x.Email,
                    z => z.ResolveUsing(x => string.IsNullOrWhiteSpace(x.Email) ? "не указан" : x.Email))
                .ForMember(x => x.PhoneNumber,
                    z => z.ResolveUsing(x => string.IsNullOrWhiteSpace(x.PhoneNumber) ? "не указан" : x.PhoneNumber));

            Mapper.CreateMap<ApplicationUser, ProfileViewModel>().BothWays();

            Mapper.CreateMap<WaitingUser, WaitingUserView>()
                .ForMember(x => x.Position, z => z.ResolveUsing(c => (int)c.Position))
                .ReverseMap()
                .ForMember(x => x.Position, z => z.ResolveUsing(c => (OrganizationPosition)c.Position));

        }
    }
}
