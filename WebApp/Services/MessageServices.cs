﻿
using System.Threading.Tasks;
using MailKit.Net.Smtp;
using MailKit.Security;
using MimeKit;

namespace WebApp.Services
{
    // This class is used by the application to send Email and SMS
    // when you turn on two-factor authentication in ASP.NET Identity.
    // For more details see this link http://go.microsoft.com/fwlink/?LinkID=532713
    public class AuthMessageSender : IEmailSender, ISmsSender
    {
        private readonly IMyLogger _logger;

        public AuthMessageSender(IMyLogger logger)
        {
            _logger = logger;
        }

        public async Task SendEmailAsync(string email, string subject, string message)
        {
            var emailMessage = new MimeMessage();
            emailMessage.From.Add(new MailboxAddress("Jhon Doe", "shablonator@yandex.ru"));
            emailMessage.To.Add(new MailboxAddress("", email));
            emailMessage.Subject = subject;
            emailMessage.Body = new TextPart("plain") { Text = message };
            _logger.Log(" -- ");
            _logger.Log("отправляю на");
            _logger.Log(email);
            using (var client = new SmtpClient())
            {
                

                await client.ConnectAsync("smtp.yandex.ru", 587, SecureSocketOptions.StartTls);
                client.Authenticate("shablonator@yandex.ru", "zx1343dsfxcc");    
                await client.SendAsync(emailMessage).ConfigureAwait(false);
                await client.DisconnectAsync(true).ConfigureAwait(false);
            }
            _logger.Log("отправлено");


            //using (var msg = new MailMessage("jhonDoe007@yandex.ru", email, subject, message))
            //{
            //    var smtpClient = new SmtpClient("smtp.yandex.ru", 587)
            //    {
            //        Credentials = new NetworkCredential("jhonDoe007", "jhonDoe007123"),
            //        EnableSsl = true
            //    };
            //    smtpClient.Send(msg);
            //}
            //jhonDoe007@yandex.ru
            //jhonDoe007
            //jhonDoe007123
            // Plug in your email service here to send an email.
        }

        public Task SendSmsAsync(string number, string message)
        {
            // Plug in your SMS service here to send a text message.
            return Task.FromResult(0);
        }
    }
}
