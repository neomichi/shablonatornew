﻿using System.Threading.Tasks;
using WebApp.Models;

namespace WebApp.Services
{
    public interface IGoogleCalendar
    {
     
        /// <summary>
        /// удалить событие
        /// </summary>
        /// <param name="auctionProfile">аукцион</param>
        /// <returns></returns>
        Task Remove(AuctionProfile auctionProfile);
        /// <summary>
        /// если нет добавить, если существует то обновить
        /// </summary>
        /// <param name="auctionProfile"></param>
        /// <returns></returns>
        Task AddOrUpdate(AuctionProfile auctionProfile);
    }
}