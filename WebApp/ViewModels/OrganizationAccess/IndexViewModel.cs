﻿using System;
using System.Collections.Generic;

namespace WebApp.ViewModels.OrganizationAccess
{
    public class IndexViewModel
    {
        public IndexViewModel()
        {
            Managers = new List<string>();
            Admins = new List<string>();
            Signatories = new List<string>();
            Responsibles = new List<string>();
        }
        public List<string> Managers { get; set; }
        public List<string> Admins { get; set; }
        public List<string> Signatories { get; set; }
        public List<string> Responsibles  { get; set; }
        public string OrganizationName { get; set; }
        public Guid OrganizationId { get; set; }
    }
}
