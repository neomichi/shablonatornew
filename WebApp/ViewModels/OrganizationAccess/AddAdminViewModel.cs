﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApp.ViewModels.OrganizationAccess
{
    public class AddAdminViewModel
    {
        [Display(Name = "Адрес Email")]
        [Required(ErrorMessage = "Необходим адрес Email")]
        [EmailAddress(ErrorMessage = "Неправильный адрес Email")]
        public string Email { get; set; }
        public Guid OrganizationId { get; set; }
    }
}
