﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApp.ViewModels.Auction
{
    public class AuctionProfileView
    {
        public Guid AuctionProfileDtoId { get; set; }
        [Display(Name = "Дата начала аукциона")]
        public DateTime? AuctionDateStart { get; set; }
        [Display(Name = "Дата окончания аукциона")]
        public DateTime? AuctionDateEnd { get; set; }
        [Display(Name = "Процент обеспечения заявки")]
        public double? ApplicationCoveragePercentage { get; set; }
        [Display(Name = "Процент обеспечения контракта")]
        public double? ContractCoveragePercentage { get; set; }
        [Display(Name = "Условия обеспечения для коммерческих компаний")]
        public string TermsForCommercialCompanies { get; set; }
        [Display(Name = "Условия обеспечения для государственных команий")]
        public string TermsForGovermentCompanies { get; set; }
        [Display(Name = "Дата рассмотрения N части заявки")]
        public DateTime? DateOfReview { get; set; }
        [Display(Name = "Дата проведения аукциона")]
        public DateTime? AuctionDate { get; set; }
        [Display(Name = "Дата начала разъяснений")]
        public DateTime? ClarificationDateStart { get; set; }
        [Display(Name = "Дата окончания разъяснений")]
        public DateTime? ClarificationDateEnd { get; set; }
        [Display(Name = "Электронная площадка торгов")]
        public string ElectronicTradingPlatform { get; set; }
        [Display(Name = "Требование об отсутствии в предусмотренном Федеральным законом №44-ФЗ от 05.04.2013 реестре недобросовестных поставщиков")]
        public string TheRequirementInTheAbsenceOfTheFederalLaw44 { get; set; }

        [Display(Name = "Требование об отсутствии в предусмотренном порядке  до вступления в силу Федеральным законом №44-ФЗ от 05.04.2013 реестре недобросовестных поставщиков")]
        public string BeforeOfTheFederalLaw44 { get; set; }
        [Display(Name = "Преимущества участников")]
        public string BenefitsOfParticipating { get; set; }
        [Display(Name = "Допуск товара из иностранного государства")]
        public string AdmissionFromForeigns { get; set; }
        [Display(Name = "Срок подписания контракта")]
        public string DeadlineForSigning { get; set; }
        [Display(Name = "Документы после выполнения констракта")]
        public string DocumentsAfterContract { get; set; }

      

    }
}
