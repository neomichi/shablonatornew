﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebApp.Models;

namespace WebApp.ViewModels.Auction
{
    public class AuctionView
    {

        [Display(Name = "Привязка к организации")]
        public string OrganizationName { get; set; }

        public IEnumerable<SelectListItem> Organizations { get; set; }

        public Guid AuctionProfileId { get; set; }
        [Display(Name = "Дата начала аукциона")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.MM.yyyy HH:mm}")]
        public DateTime? AuctionDateStart { get; set; }
        [Display(Name = "Дата окончания аукциона")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.MM.yyyy HH:mm}")]
        public DateTime? AuctionDateEnd { get; set; }
        [Display(Name = "Процент обеспечения заявки")]
        [RegularExpression(Code.Constant.RegexCheckDoubleValue, ErrorMessage = "нужно указать численное значение")]

        public double? ApplicationCoveragePercentage { get; set; }
        [Display(Name = "Процент обеспечения контракта")]
        [RegularExpression(Code.Constant.RegexCheckDoubleValue, ErrorMessage = "нужно указать численное значение")]
        public double? ContractCoveragePercentage { get; set; }
        [Display(Name = "Условия обеспечения для коммерческих компаний")]
        public string TermsForCommercialCompanies { get; set; }
        [Display(Name = "Условия обеспечения для государственных команий")]
        public string TermsForGovermentCompanies { get; set; }
        [Display(Name = "Дата рассмотрения N части заявки")]
        public DateTime? DateOfReview { get; set; }
        [Display(Name = "Дата проведения аукциона")]
        public DateTime? AuctionDate { get; set; }
        [Display(Name = "Дата начала разъяснений")]
        public DateTime? ClarificationDateStart { get; set; }
        [Display(Name = "Дата окончания разъяснений")]
        public DateTime? ClarificationDateEnd { get; set; }
        [Display(Name = "Электронная площадка торгов")]
        public string ElectronicTradingPlatform { get; set; }
        [Display(Name = "Требование об отсутствии в предусмотренном Федеральным законом №44-ФЗ от 05.04.2013 реестре недобросовестных поставщиков")]
        public string TheRequirementInTheAbsenceOfTheFederalLaw44 { get; set; }

        [Display(Name = "Требование об отсутствии в предусмотренном порядке  до вступления в силу Федеральным законом №44-ФЗ от 05.04.2013 реестре недобросовестных поставщиков")]
        public string BeforeOfTheFederalLaw44 { get; set; }
        [Display(Name = "Преимущества участников")]
        public string BenefitsOfParticipating { get; set; }
        [Display(Name = "Допуск товара из иностранного государства")]
        public string AdmissionFromForeigns { get; set; }
        [Display(Name = "Срок подписания контракта")]
        public string DeadlineForSigning { get; set; }
        [Display(Name = "Документы после выполнения констракта")]
        public string DocumentsAfterContract { get; set; }

        [Display(Name = "Привязка к организации")]
        [ForeignKey("Organization")]
        public Guid? OrganizationId { get; set; }
        public virtual Organization Organization { get; set; }
        public virtual PurchaseProfile PurchaseProfile { get; set; }

    }
}
