﻿using System.ComponentModel.DataAnnotations;

namespace WebApp.ViewModels.Profile
{
    public class ProfileViewModel
    {
        public string Id { get; set; }
        [Display(Name = "ФИО")]
        public string FullName { get; set; }
        [Display(Name = "Должность")]
        public string Position { get; set; }
        [Display(Name = "Контактный телефон")]
        public string Phone { get; set; }
        //[Display(Name = "Электронный адрес")]
        //public string Email { get; set; }
        [Display(Name = "Аватар")]
        public string Picture { get; set; }
        [Display(Name = "Профиль в социальных сетях")]
        public string SocialProfile { get; set; }
        [Display(Name = "Доверенность №")]
        public string PowerOfAttorneyNumber { get; set; }

        public string ImgInput { get; set; }

    }
}
