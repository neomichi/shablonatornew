﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.ViewModels.WaitingUser
{
    public class WaitingUserView
    {
        public string BackUrl { get; set; }
        public string FromUserId { get; set; }
        public string InviteEmail { get; set; }
        public Guid OrganizationId { get; set; }
        public int Position { get; set; }

        public bool FirstSend { get; set; }

        public WaitingUserView()
        {
            FirstSend = true;
        }
     }
}
