﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.ViewModels.Notification
{
    public class SendMail
    {
        public string Email { get; set; }
        public string Message { get; set; }
        public string Subject { get; set; }
    }
}
