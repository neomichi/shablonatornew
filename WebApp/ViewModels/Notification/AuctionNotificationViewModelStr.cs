﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApp.ViewModels.Notification
{
    public class AuctionNotificationViewModelStr
    {
        #region Customer
        [Display(Name = "Наименование")]
        public string CustomerName { get; set; }
        [Display(Name = "Место нахождения (почтовый адрес)")]
        public string CustomerAddres { get; set; }
        [Display(Name = "Адрес электронной почты")]
        public string CustomerEmail { get; set; }
        [Display(Name = "Контактный телефон")]
        public string CustomerPhone { get; set; }
        [Display(Name = "Ответственный ")]
        public string CustomerResponsible { get; set; }
        [Display(Name = "Исполнитель")]
        public string CustomerPerformer { get; set; }
        #endregion
        #region Предмет закупки
        [Display(Name = "наименование прелмета закупки")]
        public string PurchaseName { get; set; }
        [Display(Name = "Количество")]
        public string PurchaseCount { get; set; }
        [Display(Name = "Место (адрес) поставки")]
        public string PurchaseAddres { get; set; }
        [Display(Name = "Срок поставки ")]
        public string PurchaseDateDelivery { get; set; }
        [Display(Name = "Начальная цена")]
        public string PurchasePrice { get; set; }
        [Display(Name = "Источник финансирования")]
        public string SourceOfFinancing { get; set; }
        [Display(Name = "Требования к содержанию и составу заявки на участие в электронном аукционе")]
        public string RequirementsForContent { get; set; }
        #endregion
        [Display(Name = "Идентификационный код закупки")]
        public string PurchasesIdentificationCode { get; set; }
        [Display(Name = "Ограничение участия")]
        public string LimitationParticipation { get; set; }
        [Display(Name = "Электронный аукцион")]
        public string MethodDeterminingSupplier { get; set; }
        [Display(Name = "Дата и время окончания срока подачи заявок")]
        public string DeadlineForSubmission { get; set; }
        #region Условия обеспечения заявок
        [Display(Name = "размер (%) процент исчисляется от НМЦК.")]
        public string SizeCalculated { get; set; }
        [Display(Name = "Сумма")]
        public string Summ { get; set; }
        [Display(Name = "Порядок предоставления")]
        public string ProcedureGranting { get; set; }
        [Display(Name = "Реквизиты счета")]
        public string AccountDetails { get; set; }
        #endregion
        #region Условия обеспечения контракта
        [Display(Name = "Размер процент исчисляется от НМЦК")]
        public string Size { get; set; }
        [Display(Name = "Порядок предоставления")]
        public string ProcedureGrantingContract { get; set; }
        [Display(Name = "Срок предоставления")]
        public string DateGrantingContract { get; set; }
        [Display(Name = "Требования к обеспечению")]
        public string RequirementsFor { get; set; }
        #endregion
        [Display(Name = "Реквизиты счета для перечисления денежных средств в качестве обеспечения исполнения контракта")]
        public string BankAccountTransferPerformanceContract { get; set; }
        [Display(Name = "Условия банковской гарантии, представляемой в качестве обеспечения исполнения контракта")]
        public string TermsBankGuarantePerformanceContract { get; set; }
        [Display(Name = "Срок действия банковской гарантии, представляемой в качестве обеспечения исполнения контракта")]
        public string ValidityBankGuarantee { get; set; }
        [Display(Name = "Требования к участникам")]
        public string RequirementsParticipants { get; set; }
        [Display(Name = "Адрес электронной площадки")]
        public string EmailSite { get; set; }
        [Display(Name = "Требование к составу первых и вторых частей")]
        public string RequirementsCompositionParts { get; set; }
        [Display(Name = "Дата окончания срока рассмотрения 1-х частей заявок")]
        public string DateEndFirst { get; set; }
        [Display(Name = "Дата проведения аукциона")]
        public string DateAuction { get; set; }
        [Display(Name = "Преимущества участникам")]
        public string BenefitsParticipants { get; set; }
        [Display(Name = "Условия допуска товаров из иностранного государства")]
        public string TermsAdmissionGoodsForeignCountry { get; set; }
    }
}
