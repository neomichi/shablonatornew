﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.ViewModels.Notification
{
    public class InformationAuctionCard 
    {

        #region  Заказчие
        [Display(Name = "Наименование")]
        public string CustomerName { get; set; }

        [Display(Name = "Место нахождения (почтовый адрес)")]
        public string CustomerAddres { get; set; }

        [Display(Name = "Адрес электронной почты")]
        public string CustomerEmail { get; set; }


        [Display(Name = "Контактный телефон")]
        public string CustomerPhone { get; set; }

        [Display(Name = "Контрактная служба (руководитель)")]
        public string CustomerServiceСhief { get; set; }

        [Display(Name = "контактный телефон, ответственный")]
        public string CustomerResponsible { get; set; }

        [Display(Name = "исполнитель (инициатор закупки)")]
        public string CustomerInitiator { get; set; }

        #endregion


        #region Предмет закупки

        [Display(Name = "наименование")]
        public string PurchaseName { get; set; }

        [Display(Name = "описание")]
        public string PurchaseDescription { get; set; }


        [Display(Name = "качество")]
        public string PurchaseQuality { get; set; }


        [Display(Name = "гарантийные обязательства")]
        public string PurchaseWarranty { get; set; }


        [Display(Name = "место (адрес) поставки")]
        public string PurchaseDeliveryAddress { get; set; }

        [Display(Name = "срок поставки")]
        public DateTime PurchaseDeliveryTime { get; set; }


        [Display(Name = "начальная цена")]
        public decimal PurchaseStartPrice { get; set; }

        [Display(Name = "обоснование начальной цены")]
        public string PurchaseStartPriceDescription { get; set; }

        [Display(Name = "обоснование начальной цены")]
        public string PurchasePriceComposition { get; set; }


        [Display(Name = "порядок оплаты")]
        public string PurchasePaymentOrder { get; set; }


        [Display(Name = "информация о валюте")]
        public string PurchaseCurrencyInformation { get; set; }

        [Display(Name = "Порядок применения официального " +
                        "курса иностранной валюты к рублю " +
                        "Российской Федерации, установленного " +
                        "Центральным банком Российской Федерации и " +
                        "используемого при оплате контракта")]
        public string PurchaseCurrencyRulesOfMethod { get; set; }


        [Display(Name = "срок подписания контракта")]
        public DateTime PurchaseTimeSigningContract { get; set; }

        [Display(Name = "проект контракта")]
        public string PurchaseProject { get; set; }
        #endregion

        [Display(Name = "Идентификационный код закупки")]
        public string PurchasesIdentificationCode { get; set; }

        [Display(Name = "Ограничение участия")]
        public string LimitationParticipation { get; set; }


        [Display(Name = "Способ определения поставщика")]
        public string FindMethodProvider { get; set; }

        [Display(Name = "Дата и время окончания срока подачи заявок ")]
        public DateTime DeadlineForSubmission { get; set; }


        #region	Условия обеспечения заявок

        [Display(Name = "размер (%) процент исчисляется от НМЦК")]
        public Decimal PercentNmck { get; set; }
        
        [Display(Name = "сумма (руб.)")]
        public Decimal Summ { get; set; }

        [Display(Name = "Порядок предоставления")]
        public string ProcedureGranting { get; set; }

        [Display(Name = "Реквизиты счета")]
        public string AccountDetails { get; set; }
        #endregion

        #region Условия обеспечения контракта

        [Display(Name = "размер (%) процент исчисляется от НМЦК")]
        public Decimal ConditionsPercentNmck { get; set; }

        [Display(Name = "Порядок предоставления")]
        public string ProcedureGrantingContract { get; set; }

        [Display(Name = "Срок предоставления")]
        public string DateGrantingContract { get; set; }

        [Display(Name = "Требования к обеспечению")]
        public string RequirementsFor { get; set; }

        [Display(Name = "Реквизиты счета для перечисления денежных средств в качестве обеспечения исполнения контракта")]
        public string BankAccountTransferPerformanceContract { get; set; }

        [Display(Name = "Условия банковской гарантии, представляемой в качестве обеспечения исполнения контракта")]
        public string TermsBankGuarantePerformanceContract { get; set; }

        [Display(Name = "Срок действия банковской гарантии, представляемой в качестве обеспечения исполнения контракта")]
        public string ValidityBankGuarantee { get; set; }

        #endregion

        #region 8.Требования к участникам

        [Display(Name = "перечень требований")]
        public string RequirementsForAllowUsers { get; set; }

        #endregion

        [Display(Name = "Адрес электронной площадки")]
        public string EmailSite { get; set; }

        [Display(Name = "Дата окончания срока рассмотрения 1-х частей заявок")]
        public DateTime DateEndFirst { get; set; }

        [Display(Name = "Преимущества участникам")]
        public string BenefitsParticipants { get; set; }

        [Display(Name = "Условия допуска товаров из иностранного государства")]
        public string TermsAdmissionGoodsForeignCountry { get; set; }


        [Display(Name = "Предоставление разъяснений документации участникам")]
        public string MemberClarification { get; set; }

        [Display(Name = "дата начала")]
        public DateTime MemberClarificationDateStart { get; set; }

        [Display(Name = "дата окончания")]
        public DateTime MemberClarificationDateEnd { get; set; }


        [Display(Name = "Информация о возможности одностороннего отказа от исполнения контракта")]
        public string InformationAboutRefusal { get; set; }

        [Display(Name = "Перечень необходимых документов")]
        public string RequirementForDocuments { get; set; }

        [Display(Name = "Требование об отсутствии в предусмотренном" +
                 " Федеральным законом №44-ФЗ от 05.04.2013" +
                 " реестре недобросовестных поставщиков (подрядчиков," +
                 " исполнителей) информации об участнике закупки, " +
                 "в том числе информации об учредителях, о членах коллегиального " +
                 "исполнительного органа, лице, исполняющем функции единоличного " +
                 "исполнительного органа участника закупки - юридического лица")]

        public string RequirementOfNoInformation { get; set; }

        [Display(Name = "Требование об отсутствии сведений об участнике электронного" +
                        " аукциона в реестре недобросовестных поставщиков," +
                        " сформированном в порядке, действовавшем до дня вступления" +
                        " в силу Федерального закона РФ от 05.04.2013 № 44-ФЗ «О Контрактной " +
                        "системе в сфере закупок товаров, работ, услуг для обеспечения " +
                        "государственных и муниципальных нужд")]
        public string RequirementOfNoInformationAboutAuction { get; set; }
    }
}
