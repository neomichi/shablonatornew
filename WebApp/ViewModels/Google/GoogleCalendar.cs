﻿namespace WebApp.ViewModels.Google
{
    public class GoogleOptions
    {
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string CalendarName { get; set; }
        public string GoogleApplicationName { get; set; }
    }
}
