﻿using System.Collections.Generic;
using WebApp.Models;

namespace WebApp.ViewModels.Organizations
{
    public class IndexViewModel
    {
        public IndexViewModel()
        {
            AsAdmin = new List<Organization>();
            AsManager = new List<Organization>();
            AsResponsible = new List<Organization>();
            AsSignatory = new List<Organization>();
        }
        public List<Organization> AsAdmin { get; set; }
        public List<Organization> AsManager { get; set; }
        public List<Organization> AsResponsible { get; set; }
        public List<Organization> AsSignatory { get; set; }
    }
}
