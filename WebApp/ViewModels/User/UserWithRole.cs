﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebApp.ViewModels.User
{
    public class UserWithRole
    {
        [Display(Name = "#")]
        public string Id { get; set; }

        [Display(Name = "Аватар")]
        public string Picture { get; set; }

        [Display(Name = "имя")]
        public string FullName { get; set; }

        [Display(Name = "email")]
        public string Email { get; set; }

        [Display(Name = "телефон")]
        public string PhoneNumber { get; set; }
        [Display(Name = "роли")]
        public string Roles { get; set; }

        public string RemoveLink { get; set; }
        public string EditRoleLink { get; set; }

        public IEnumerable<SelectListItem> AllRoles { get; set; }
    }
}
