﻿function run(url, document) {
    $.getJSON(url, function (data) {
        console.log(data);
        if (data instanceof Array) {
            showSmallModal(data);
        } else {
           WriteDocFile(document, data);
        }
    }).fail(function () {
        showSmallModal("ошибка получения данных");
    });
    return false;
}

function showSmallModal(text) {
    var html = '<ul>';
    $.each(text, function (index, value) {
        html += '<li>' + value + '</li>';
      
    });
    html += '</ul>';

    $('#smallmodal .modal-body').html(html);
    $('#smallmodal').modal('show');
}



function WriteDocFile(file, data) {
    var loadFile = function (url, callback) {
        JSZipUtils.getBinaryContent(url, callback);
    }
    loadFile(file, function (err, content) {
        var doc = new Docxgen(content);
        doc.setData(data);
        doc.render();
        var output = doc.getZip().generate({ type: "blob" });
        saveAs(output, "output.docx");
    });
}