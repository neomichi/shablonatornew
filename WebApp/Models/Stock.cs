﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class Stock
    {
        public Guid StockId { get; set; }
        [Display(Name = "Описание места выгрузки/погрузки")]
        public string LoadingPlace { get; set; }
        [Display(Name = "Адрес выгрузки/погрузки")]
        public string LoadingAddress { get; set; }
        [Display(Name = "Условия заезда/выезда")]
        public string CheckinTerms { get; set; }
        [Display(Name = "Контакты отвественного за приемку/погрузку")]
        public string Contacts { get; set; }
        [Display(Name = "ФИО отвественного за приемку/погрузку")]
        public string NameOfResponsibility { get; set; }
    }
}
