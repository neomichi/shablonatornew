﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Models
{
    public class Organization
    {
        public Organization()
        {
            OrganizationToManagers = new List<OrganizationToManager>();
            AuctionProfiles=new List<AuctionProfile>();
        }
        public Guid OrganizationId { get; set; }
        [Display(Name="Название")]
        public string Name { get; set; }

        public Guid? OrganizationProfileId { get; set; }
        public OrganizationProfile OrganizationProfile { get; set; }

        public Guid? OrganizationBankDetailsId { get; set; }
        public OrganizationBankDetails OrganizationBankDetails { get; set; }

        public Guid? OrganizationProvisionRecipientBankDetailsId { get; set; }
        public OrganizationProvisionRecipientBankDetails OrganizationProvisionRecipientBankDetails { get; set; }

        public Guid? StockId { get; set; }
        public Stock Stock { get; set; }

        public List<OrganizationToManager> OrganizationToManagers { get; set; }
        public List<OrganizationToAdmin> Holders { get; set; }
        public List<OrganizationToSignatory> Signatories { get; set; }
        public List<OrganizationToResponsible> Responsibles{ get; set; }
        

        public List<AuctionProfile> AuctionProfiles { get; set; }
      
    }
}


