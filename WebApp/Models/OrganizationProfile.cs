﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Models
{
    public class OrganizationProfile
    {
        public Guid OrganizationProfileId { get; set; }

        [Display(Name = "Юридическое лицо")]
        public string RegistredName { get; set; }

        [Display(Name = "Наименование организации")]
        public string Name { get; set; }

        [Display(Name = "Индекс месторасположения")]
        public string PostIndex { get; set; }

        [Display(Name = "Регион")]
        public string Region { get; set; }

        [Display(Name = "Населенный пункт")]
        public string Location { get; set; }

        [Display(Name = "Улица")]
        public string Street { get; set; }

        [Display(Name = "Дом")]
        public string Apartment { get; set; }

        [Display(Name = "Офис")]
        public string Office { get; set; }

        [Display(Name = "Телефон")]
        public string Phone { get; set; }

        [Display(Name = "Факс")]
        public string Fax { get; set; }

        [Display(Name = "Электронная почта")]
        public string Email { get; set; }

        [Display(Name = "Сайт")]
        public string Site { get; set; }

        [Display(Name = "Ссылка на логотип")]
        public string LinkLogo { get; set; }

        [Display(Name = "Режим работы")]
        public string WorkingHours{ get; set; }

        [Display(Name = "Перерыв на обед")]
        public string ClosedForLunch { get; set; }

        [Display(Name = "Источник финансирования")]
        public string SourceOfFinancing { get; set; }

    }
}
