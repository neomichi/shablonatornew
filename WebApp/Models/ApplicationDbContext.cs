﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Npgsql.EntityFrameworkCore.PostgreSQL;

namespace WebApp.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
    

            builder.HasPostgresExtension("uuid-ossp");

            base.OnModelCreating(builder);



            //Many to many for managers
            builder.Entity<OrganizationToManager>()
                .HasKey(t => new { t.OrganizationId, t.ApplicationUserId });

            builder.Entity<OrganizationToManager>()
                .HasOne(pt => pt.ApplicationUser)
                .WithMany(p => p.ManagerFor)
                .HasForeignKey(pt => pt.ApplicationUserId);

            builder.Entity<OrganizationToManager>()
                .HasOne(pt => pt.Organization)
                .WithMany(t => t.OrganizationToManagers)
                .HasForeignKey(pt => pt.OrganizationId);

            //Many to many for Admins
            builder.Entity<OrganizationToAdmin>()
                .HasKey(t => new { t.OrganizationId, t.ApplicationUserId });

            builder.Entity<OrganizationToAdmin>()
                .HasOne(pt => pt.ApplicationUser)
                .WithMany(p => p.AdminFor)
                .HasForeignKey(pt => pt.ApplicationUserId);

            builder.Entity<OrganizationToAdmin>()
                .HasOne(pt => pt.Organization)
                .WithMany(t => t.Holders)
                .HasForeignKey(pt => pt.OrganizationId);

            //Many to many for Signatories
            builder.Entity<OrganizationToSignatory>()
                .HasKey(t => new { t.OrganizationId, t.ApplicationUserId });

            builder.Entity<OrganizationToSignatory>()
                .HasOne(pt => pt.ApplicationUser)
                .WithMany(p => p.SignatoryFor)
                .HasForeignKey(pt => pt.ApplicationUserId);

            builder.Entity<OrganizationToSignatory>()
                .HasOne(pt => pt.Organization)
                .WithMany(t => t.Signatories)
                .HasForeignKey(pt => pt.OrganizationId);

            //Many to many for Responsibles
            builder.Entity<OrganizationToResponsible>()
                .HasKey(t => new { t.OrganizationId, t.ApplicationUserId });

            builder.Entity<OrganizationToResponsible>()
                .HasOne(pt => pt.ApplicationUser)
                .WithMany(p => p.ResponsibleFor)
                .HasForeignKey(pt => pt.ApplicationUserId);

            builder.Entity<OrganizationToResponsible>()
                .HasOne(pt => pt.Organization)
                .WithMany(t => t.Responsibles)
                .HasForeignKey(pt => pt.OrganizationId);

            builder.Entity<AuctionProfile>()
              .HasOne(p => p.Organization)
              .WithMany(b => b.AuctionProfiles)
              .HasForeignKey(p => p.OrganizationId);


            builder.Entity<PurchaseProfile>()
                    .HasIndex(b => b.AuctionProfileId)
                    .IsUnique();

            builder.Entity<WaitingUser>()
                .HasKey(t => new {t.FromUserId, t.OrganizationId, t.InviteEmail});
                    
                


            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }
        public DbSet<AuctionProfile> AuctionProfiles { get; set; }
        public DbSet<PurchaseProfile> PurchaseProfiles { get; set; }

        public DbSet<Organization> Organizations { get; set; }
        public DbSet<OrganizationToAdmin> OrganizationToAdmins { get; set; }
        public DbSet<OrganizationToManager> OrganizationToManagers { get; set; }
        public DbSet<OrganizationToSignatory> OrganizationToSignatories { get; set; }
        public DbSet<OrganizationToResponsible> OrganizationToResponsibles { get; set; }
        public DbSet<OrganizationProfile> OrganizationProfiles { get; set; }
        public DbSet<OrganizationBankDetails> OrganizationBankDetails { get; set; }
        public DbSet<OrganizationProvisionRecipientBankDetails> OrganizationProvisionRecipientBankDetails { get; set; }
        public DbSet<Stock> Stocks { get; set; }
        public DbSet<WaitingUser> WaitingUsers { get; set; }
        
    }
}
