﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Code;

namespace WebApp.Models
{
    public class WaitingUser
    {

        [ForeignKey("FromUser")]
        public string FromUserId { get; set; }
        public virtual ApplicationUser FromUser { get; set; }
        [Required]
        public string InviteEmail { get; set; }
        [ForeignKey("Organization")]
        public Guid OrganizationId { get; set; }
        public virtual Organization Organization { get; set; }
        public OrganizationPosition Position { get; set; }

        public override bool Equals(object obj)
        {
            return (obj is WaitingUser) &&
                   ((WaitingUser)obj).FromUserId.Eq(FromUserId)
                   && ((WaitingUser)obj).OrganizationId.Eq(OrganizationId)
                   && ((WaitingUser) obj).InviteEmail.Eq(InviteEmail);
        }
    }
}
