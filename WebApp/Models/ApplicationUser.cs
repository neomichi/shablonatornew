﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace WebApp.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public List<OrganizationToAdmin> AdminFor { get; set; }
        public List<OrganizationToManager> ManagerFor { get; set; }
        public List<OrganizationToResponsible> ResponsibleFor { get; set; }
        public List<OrganizationToSignatory> SignatoryFor { get; set; }
        public string FullName { get; set; }
        public string Position { get; set; }
        public string Picture { get; set; }
        public string SocialProfile { get; set; }
        public string PowerOfAttorneyNumber { get; set; }

        public ApplicationUser()
        {
            Picture = "/images/default.jpg";
        }
    }
}



