﻿using System;

namespace WebApp.Models
{
    public class OrganizationToResponsible
    {
        public Guid OrganizationId { get; set; }
        public Organization Organization { get; set; }
        public string ApplicationUserId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
    }
}
