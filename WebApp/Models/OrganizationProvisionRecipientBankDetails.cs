﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Models
{
    public class OrganizationProvisionRecipientBankDetails
    {
        public Guid OrganizationProvisionRecipientBankDetailsId { get; set; }

        [Display(Name = "Инн")]
        public string Inn { get; set; }

        [Display(Name = "КПП")]
        public string Kpp { get; set; }

        [Display(Name = "ОГРН")]
        public string Ogrn { get; set; }

        [Display(Name = "ОКПО")]
        public string Okpo { get; set; }

        [Display(Name = "ОКВЭД")]
        public string Okved { get; set; }

        [Display(Name = "Наименование банка, отделения")]
        public string BankName { get; set; }

        [Display(Name = "Расчетный счет")]
        public string BankAccount { get; set; }

        [Display(Name = "БИК")]
        public string Bik { get; set; }

        [Display(Name = "Корреспонденский счет")]
        public string CorrespondentAccount { get; set; }

        [Display(Name = "Лицевой счет")]
        public string PersonalAccount { get; set; }

        [Display(Name = "Юридический адрес")]
        public string RegistredOffice { get; set; }

        [Display(Name = "Назначение платежа")]
        public string PurposeOfPayment { get; set; }

        [Display(Name = "КБК")]
        public string Kbk { get; set; }
    }
}
