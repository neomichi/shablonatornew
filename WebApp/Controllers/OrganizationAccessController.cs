﻿using System.Linq;
using System.Threading.Tasks;
using WebApp.Models;
using System;
using WebApp.ViewModels.OrganizationAccess;
using System.Security.Claims;
using System.Text;
using System.Threading;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using WebApp.Code;
using WebApp.Services;
using WebApp.ViewModels.WaitingUser;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Controllers
{
    public class OrganizationAccessController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IEmailSender _emailSender;
        private readonly IMyLogger _logger; 

     
        public OrganizationAccessController(ApplicationDbContext context, IEmailSender emailSender, IMyLogger logger)
        {
            _context = context;
            _emailSender = emailSender;
            _logger = logger;
        }

        public async Task<IActionResult> Index(Guid? id)
        {
           
            var model = new IndexViewModel();
            var organization = await _context.Organizations.FirstOrDefaultAsync(x => x.OrganizationId == id);
            model.OrganizationName = organization.Name;
            model.OrganizationId = organization.OrganizationId;

            model.Admins = await _context.OrganizationToAdmins.Where(x => x.OrganizationId == id)
                .Select(x => x.ApplicationUser)
                .Where(x => x.Id != User.FindFirst(ClaimTypes.NameIdentifier).Value)
                .Select(x => x.Email)
                .ToListAsync();

            model.Signatories = await _context.OrganizationToSignatories.Where(x => x.OrganizationId == id)
                .Select(x => x.ApplicationUser)
                .Where(x => x.Id != User.FindFirst(ClaimTypes.NameIdentifier).Value)
                .Select(x => x.Email)
                .ToListAsync();

            model.Responsibles = await _context.OrganizationToResponsibles.Where(x => x.OrganizationId == id)
                .Select(x => x.ApplicationUser)
                .Where(x => x.Id != User.FindFirst(ClaimTypes.NameIdentifier).Value)
                .Select(x => x.Email)
                .ToListAsync();

            model.Managers = await _context.OrganizationToManagers.Where(x => x.OrganizationId == id)
                .Select(x => x.ApplicationUser)
                .Where(x => x.Id != User.FindFirst(ClaimTypes.NameIdentifier).Value)
                .Select(x => x.Email)
                .ToListAsync();

            return View(model);
        }

        public ActionResult AddAdmin(Guid id)
        {
            var model = new AddAdminViewModel
            {
                OrganizationId = id
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddAdmin(AddAdminViewModel model)
        {
            if (ModelState.IsValid)
            {

             

             

                var user =
                   await
                       _context.Users.FirstOrDefaultAsync(
                           x => x.NormalizedEmail.Equals(model.Email, StringComparison.OrdinalIgnoreCase));

                if (user == null)
                {
                    return View("AddWaitingUser", new WaitingUserView()
                    {
                        FromUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value,
                        InviteEmail = model.Email,
                        OrganizationId = model.OrganizationId,
                        Position = (int)OrganizationPosition.Admin,
                        BackUrl = HttpContext.Request.Path.Value

                    });
                }
                else
                {
                    _context.OrganizationToAdmins.Add(new OrganizationToAdmin
                    {
                        OrganizationId = model.OrganizationId,
                        ApplicationUserId = user.Id
                    });
                }
                await _context.SaveChangesAsync();
                return RedirectToAction("Index", new { id = model.OrganizationId });
            }
            return View(model);
        }

        public ActionResult AddManager(Guid id)
        {
            var model = new AddAdminViewModel
            {
                OrganizationId = id
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddManager(AddAdminViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user =
                  await
                      _context.Users.FirstOrDefaultAsync(
                          x => x.NormalizedEmail.Equals(model.Email, StringComparison.OrdinalIgnoreCase));

                if (user == null)
                {
                    return View("AddWaitingUser", new WaitingUserView()
                    {
                        FromUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value,
                        InviteEmail = model.Email,
                        OrganizationId = model.OrganizationId,
                        Position = (int)OrganizationPosition.Manager,
                        BackUrl = HttpContext.Request.Path.Value

                    });
                }
                else
                {
                    _context.OrganizationToManagers.Add(new OrganizationToManager
                    {
                        OrganizationId = model.OrganizationId,
                        ApplicationUserId = user.Id
                    });
                }

                await _context.SaveChangesAsync();
                return RedirectToAction("Index", new { id = model.OrganizationId });
            }
            return View(model);
        }

        public ActionResult AddSignatory(Guid id)
        {
            var model = new AddAdminViewModel
            {
                OrganizationId = id
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddSignatory(AddAdminViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user =
                  await
                      _context.Users.FirstOrDefaultAsync(
                          x => x.NormalizedEmail.Equals(model.Email, StringComparison.OrdinalIgnoreCase));
                

                if (user == null)
                {
                    return View("AddWaitingUser", new WaitingUserView()
                    {
                        FromUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value,
                        InviteEmail = model.Email,
                        OrganizationId = model.OrganizationId,
                        Position = (int) OrganizationPosition.Signatory,
                        BackUrl = HttpContext.Request.Path.Value

                    });
                }
                else
                {
                    _context.OrganizationToSignatories.Add(new OrganizationToSignatory
                    {
                        OrganizationId = model.OrganizationId,
                        ApplicationUserId = user.Id
                    });
                }
                await _context.SaveChangesAsync();
                return RedirectToAction("Index", new { id = model.OrganizationId });
            }
            return View(model);
        }

        public ActionResult AddResponsible(Guid id)
        {
            var model = new AddAdminViewModel
            {
                OrganizationId = id
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddResponsible(AddAdminViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user =
                   await
                       _context.Users.FirstOrDefaultAsync(
                           x => x.NormalizedEmail.Equals(model.Email, StringComparison.OrdinalIgnoreCase));

                if (user == null)
                {  
                    return View("AddWaitingUser", new WaitingUserView()
                    {
                        FromUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value,
                        InviteEmail = model.Email,
                        OrganizationId = model.OrganizationId,
                        Position = (int)OrganizationPosition.Responsible,
                        BackUrl = HttpContext.Request.Path.Value,
                    });
                }
                else
                {
                    _context.OrganizationToResponsibles.Add(new OrganizationToResponsible
                    {
                        OrganizationId = model.OrganizationId,
                        ApplicationUserId = user.Id
                    });
                }
                await _context.SaveChangesAsync();
                return RedirectToAction("Index", new { id = model.OrganizationId });
            }
            return View(model);
        }

        public ActionResult DeleteAdmin(Guid id, string email)
        {
            var model = new AddAdminViewModel
            {
                OrganizationId = id,
                Email = email
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteAdmin(AddAdminViewModel model)
        {
            if (ModelState.IsValid)
            {
                var normalizedEmail = model.Email.ToUpper();
                var user = await _context.Users.FirstOrDefaultAsync(x => x.NormalizedEmail == normalizedEmail);
                var row = await _context.OrganizationToAdmins
                    .Where(x => x.ApplicationUserId == user.Id && x.OrganizationId == model.OrganizationId)
                    .FirstOrDefaultAsync();
                _context.OrganizationToAdmins.Remove(row);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index", new { id = model.OrganizationId });
            }
            return View(model);
        }

        public ActionResult DeleteManager(Guid id, string email)
        {
            var model = new AddAdminViewModel
            {
                OrganizationId = id,
                Email = email
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteManager(AddAdminViewModel model)
        {
            if (ModelState.IsValid)
            {
                var normalizedEmail = model.Email.ToUpper();
                var user = await _context.Users.FirstOrDefaultAsync(x => x.NormalizedEmail == normalizedEmail);
                var row = await _context.OrganizationToManagers
                    .Where(x => x.ApplicationUserId == user.Id && x.OrganizationId == model.OrganizationId)
                    .FirstOrDefaultAsync();
                _context.OrganizationToManagers.Remove(row);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index", new { id = model.OrganizationId });
            }
            return View(model);
        }

        public ActionResult DeleteSignatory(Guid id, string email)
        {
            var model = new AddAdminViewModel
            {
                OrganizationId = id,
                Email = email
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteSignatory(AddAdminViewModel model)
        {
            if (ModelState.IsValid)
            {
                var normalizedEmail = model.Email.ToUpper();
                var user = await _context.Users.FirstOrDefaultAsync(x => x.NormalizedEmail == normalizedEmail);
                var row = await _context.OrganizationToSignatories
                    .Where(x => x.ApplicationUserId == user.Id && x.OrganizationId == model.OrganizationId)
                    .FirstOrDefaultAsync();
                _context.OrganizationToSignatories.Remove(row);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index", new { id = model.OrganizationId });
            }
            return View(model);
        }

        public ActionResult DeleteResponsible(Guid id, string email)
        {
            var model = new AddAdminViewModel
            {
                OrganizationId = id,
                Email = email
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteResponsible(AddAdminViewModel model)
        {
            if (ModelState.IsValid)
            {
                var normalizedEmail = model.Email.ToUpper();
                var user = await _context.Users.FirstOrDefaultAsync(x => x.NormalizedEmail == normalizedEmail);
                var row = await _context.OrganizationToResponsibles
                    .Where(x => x.ApplicationUserId == user.Id && x.OrganizationId == model.OrganizationId)
                    .FirstOrDefaultAsync();
                _context.OrganizationToResponsibles.Remove(row);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index", new { id = model.OrganizationId });
            }
            return View(model);
        }

        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            OrganizationToAdmin organizationToUser = await _context.OrganizationToAdmins.SingleAsync(m => m.OrganizationId == id);
            if (organizationToUser == null)
            {
                return NotFound();
            }

            return View(organizationToUser);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(OrganizationToAdmin organizationToUser)
        {
            if (ModelState.IsValid)
            {
                organizationToUser.OrganizationId = Guid.NewGuid();
                _context.OrganizationToAdmins.Add(organizationToUser);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["ApplicationUserId"] = new SelectList(_context.Users, "Id", "ApplicationUser", organizationToUser.ApplicationUserId);
            ViewData["OrganizationId"] = new SelectList(_context.Organizations, "OrganizationId", "Organization", organizationToUser.OrganizationId);
            return View(organizationToUser);
        }

        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            OrganizationToAdmin organizationToUser = await _context.OrganizationToAdmins.SingleAsync(m => m.OrganizationId == id);
            if (organizationToUser == null)
            {
                return NotFound();
            }
            ViewData["ApplicationUserId"] = new SelectList(_context.Users, "Id", "ApplicationUser", organizationToUser.ApplicationUserId);
            ViewData["OrganizationId"] = new SelectList(_context.Organizations, "OrganizationId", "Organization", organizationToUser.OrganizationId);
            return View(organizationToUser);
        }

        // POST: OrganizationToUsers/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(OrganizationToAdmin organizationToUser)
        {
            if (ModelState.IsValid)
            {
                _context.Update(organizationToUser);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["ApplicationUserId"] = new SelectList(_context.Users, "Id", "ApplicationUser", organizationToUser.ApplicationUserId);
            ViewData["OrganizationId"] = new SelectList(_context.Organizations, "OrganizationId", "Organization", organizationToUser.OrganizationId);
            return View(organizationToUser);
        }

        // GET: OrganizationToUsers/Delete/5
        [ActionName("Delete")]
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            OrganizationToAdmin organizationToUser = await _context.OrganizationToAdmins.SingleAsync(m => m.OrganizationId == id);
            if (organizationToUser == null)
            {
                return NotFound();
            }

            return View(organizationToUser);
        }

        // POST: OrganizationToUsers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            OrganizationToAdmin organizationToUser = await _context.OrganizationToAdmins.SingleAsync(m => m.OrganizationId == id);
            _context.OrganizationToAdmins.Remove(organizationToUser);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddWaitingUser(WaitingUserView model)
        {

            if (ModelState.IsValid)
            {
                var organization =
                    await _context.Organizations.SingleOrDefaultAsync(x => x.OrganizationId == model.OrganizationId);

                ////todo добавить class////
                _logger.Log("собираем  письмо для приглашения");
                var token = Helper.Encode64Tostring($"{User.FindFirst(ClaimTypes.NameIdentifier).Value}--{model.InviteEmail}");
                var link = HttpContext.Request.Headers["Origin"];
                var localurl = Url.Action("RegisterWithToken", "Account", new {token = token});
             
                var fulllink =$"{link}{localurl}";
                var messageTitle = "вас пришлаем в гости";

                var messageBody = new StringBuilder();
                messageBody.AppendLine("Приглашаем Вас в гости");
                messageBody.AppendFormat("пользователь {0} ", User.Identity.Name);
                messageBody.AppendFormat("в организацию {0} на позицию '{1}' \n", organization.Name, (OrganizationPosition)model.Position);
                messageBody.AppendFormat("для входа перейдите по сылке: {0} \n", fulllink);

                _logger.Log("вызвали emailSender");
                await _emailSender.SendEmailAsync(model.InviteEmail, messageTitle, messageBody.ToString());
             
        

                var waitingUser = Mapper.Map<WaitingUserView, WaitingUser>(model);


                using (_context)
                {
                    if (!_context.WaitingUsers.Any(x=>Equals(x,waitingUser)))
                    _context.Add(waitingUser);
                    await _context.SaveChangesAsync();
                }
                _logger.Log("записали таблицу WaitingUsers");
               
            }
            return RedirectToAction("Index", new { id = model.OrganizationId });
        }


    }
}
