using System.Linq;
using System.Threading.Tasks;
using WebApp.Models;
using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Controllers
{
    public class OrganizationSummaryController : Controller
    {
        private ApplicationDbContext _context;

        public OrganizationSummaryController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: OrganizationSummary
        public async Task<IActionResult> Index(Guid? id)
        {
            var model = await _context.Organizations
                .FirstOrDefaultAsync(x=>x.OrganizationId == id);
            return View(model);
        }

        
    }
}
