using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApp.Models;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Controllers
{
    public class StocksController : Controller
    {
        private ApplicationDbContext _context;

        public StocksController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: Stocks
        public async Task<IActionResult> Index()
        {
            return View(await _context.Stocks.ToListAsync());
        }

        // GET: Stocks/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Stock stock = await _context.Stocks.FirstOrDefaultAsync(m => m.StockId == id);
            if (stock == null)
            {
                return NotFound();
            }

            return View(stock);
        }

        // GET: Stocks/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Stocks/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Stock stock)
        {
            if (ModelState.IsValid)
            {
                stock.StockId = Guid.NewGuid();
                _context.Stocks.Add(stock);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(stock);
        }

        // GET: Stocks/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Stock stock = await _context.Stocks.FirstOrDefaultAsync(m => m.StockId == id);
            if (stock == null)
            {
                stock = new Stock { StockId = (Guid)id };
                _context.Stocks.Add(stock);
                await _context.SaveChangesAsync();
            }
            return View(stock);
        }

        // POST: Stocks/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Stock stock)
        {
            if (ModelState.IsValid)
            {
                _context.Update(stock);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index", "OrganizationSummary", new { id = stock.StockId });
            }
            return View(stock);
        }

        // GET: Stocks/Delete/5
        [ActionName("Delete")]
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Stock stock = await _context.Stocks.SingleAsync(m => m.StockId == id);
            if (stock == null)
            {
                return NotFound();
            }

            return View(stock);
        }

        // POST: Stocks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            Stock stock = await _context.Stocks.SingleAsync(m => m.StockId == id);
            _context.Stocks.Remove(stock);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }
    }
}
