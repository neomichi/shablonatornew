﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Google.Apis.Calendar.v3;
using Google.Apis.Calendar.v3.Data;
using Google.Apis.Services;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebApp.Code;
using WebApp.Models;
using WebApp.Services;
using WebApp.ViewModels.Auction;
using WebApp.ViewModels.Notification;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Controllers
{
    [Authorize]
    public class AuctionController : Controller
    {
        #region constructor

        private readonly ApplicationDbContext _context;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IGoogleCalendar _calendar;

        public AuctionController(ApplicationDbContext context, SignInManager<ApplicationUser> signInManager, IGoogleCalendar calendar)
        {
            _context = context;
            _signInManager = signInManager;
            _calendar = calendar;
        }
        #endregion constructor

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            ViewBag.Id = Guid.NewGuid();
            var auctions =
                await _context.AuctionProfiles.Include(x => x.Organization).AsNoTracking().OrderByDescending(x => x.AuctionDateStart).ToListAsync();

            var auctionView = Mapper.Map<IEnumerable<AuctionProfile>, IEnumerable<AuctionView>>(auctions);
            return View(auctionView);
        }

        #region actionProfile

        [HttpGet]
        public async Task<IActionResult> EditProfile(Guid? id)
        {
            if (id.HasValue && id.Value != Guid.Empty)
            {
                var auctionView = new AuctionView { AuctionProfileId = id.Value };
                var auctionProfile = await _context.AuctionProfiles.SingleOrDefaultAsync(x => x.AuctionProfileId.Eq(id.Value));
                if (auctionProfile != null)
                {
                    auctionView = Mapper.Map<AuctionProfile, AuctionView>(auctionProfile);
                }

                var auctionOrganizationId = auctionView.OrganizationId ?? Guid.Empty;

                var userid = User.FindFirst(ClaimTypes.NameIdentifier).Value; 

                auctionView.Organizations = _context.Organizations.AsNoTracking()
                    .Where(x =>
                    x.OrganizationToManagers.Any(y => y.ApplicationUserId.Equals(userid, StringComparison.OrdinalIgnoreCase))
                    || x.Responsibles.Any(y => y.ApplicationUserId.Equals(userid, StringComparison.OrdinalIgnoreCase))
                    || x.Signatories.Any(y => y.ApplicationUserId.Equals(userid, StringComparison.OrdinalIgnoreCase))
                    || x.Holders.Any(y => y.ApplicationUserId.Equals(userid, StringComparison.OrdinalIgnoreCase)))
                    .ToList()
                        .Select(x => new SelectListItem
                        {
                            Value = x.OrganizationId.ToString(),
                            Text = x.Name,
                            Selected = x.OrganizationId.Eq(auctionOrganizationId)
                        }).ToList(); ;

                return View(auctionView);
            }
            return NotFound("404");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditProfile(AuctionView auctionView)
        {
            if (ModelState.IsValid)
            {
                var auctionProfile = Mapper.Map<AuctionView, AuctionProfile>(auctionView);

                var auctionProfileExist = _context.AuctionProfiles
                    .Any(x => x.AuctionProfileId.Eq(auctionProfile.AuctionProfileId));

                if (auctionProfileExist)
                {
                    _context.AuctionProfiles.Update(auctionProfile);
                }
                else
                {
                    _context.AuctionProfiles.Add(auctionProfile);
                }
                _context.SaveChanges();

                //если с датами в порядке, вызываем календарь
                if (auctionProfile.AuctionDateStart.HasValue &&
                    auctionProfile.AuctionDateEnd.HasValue)
                {
                  await    _calendar.AddOrUpdate(auctionProfile);
                    
                }
               
            }

            ViewBag.error = ModelState.GetErrors();
            return RedirectToAction("EditProfile", new { Id = auctionView.AuctionProfileId });
        }

        public async Task<IActionResult> DeleteProfile(Guid id)
        {
            var auctionProfileDto =
                await _context.AuctionProfiles.AsNoTracking().SingleOrDefaultAsync(x => x.AuctionProfileId.Eq(id));

            var redirectUrl = Request.Headers["Referer"];
            if (auctionProfileDto != null)
            {
                await _calendar.Remove(auctionProfileDto);

                _context.Remove(auctionProfileDto);
                await _context.SaveChangesAsync();
            }

            if (!String.IsNullOrWhiteSpace(redirectUrl)) return Redirect(redirectUrl);
            else
                return RedirectToAction("GetMyList");
        }

        #endregion actionProfile

        #region purchaseProfile

        [HttpGet]
        public IActionResult EditPurchaseProfile(Guid? id)
        {
            if (id.HasValue && id.Value != Guid.Empty)
            {
                var ap = _context.PurchaseProfiles.
                    SingleOrDefault(x => x.AuctionProfileId.Eq(id.Value)) ??
                    new PurchaseProfile();

                ap.AuctionProfileId = id.Value;
                return View(ap);
            }
            return NotFound("404");
        }

        [HttpPost]
        public async Task<IActionResult> EditPurchaseProfile(PurchaseProfile data)
        {
            if (ModelState.IsValid)
            {
                var ap = _context.AuctionProfiles.SingleOrDefault(
                            x => x.AuctionProfileId.Eq(data.AuctionProfileId));
                if (ap == null)
                {
                    ap = new AuctionProfile { AuctionProfileId = data.AuctionProfileId };
                    _context.AuctionProfiles.Add(ap);
                }
                else
                {
                    _context.AuctionProfiles.Update(ap);
                }

                if (data.PurchaseProfileId.Eq(Guid.Empty))
                {
                    ap.PurchaseProfile = new PurchaseProfile();
                    data.PurchaseProfileId = Guid.NewGuid();
                    ap.PurchaseProfile = data;
                }
                else
                {
                    _context.PurchaseProfiles.Update(data);
                }
                await _context.SaveChangesAsync();
            }
            ViewData["Error"] = ModelState.GetErrors();

            return View(data);
        }

        #endregion purchaseProfile

        /// <summary>
        /// получить извещение
        /// id= auctionprofile
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IActionResult> GetNoticeDocument(Guid id)
        {
            var errors = new List<string>();

            var ap = await _context.AuctionProfiles.AsNoTracking().SingleOrDefaultAsync(x => x.AuctionProfileId == id);

            var test = ap != null && ap.OrganizationId.HasValue;

            if (!test)
            {
                errors.Add("такого аукциона не сущесвует, либо вы не указали организацию");

            }
            else
            {
                var organization =
                    await
                        _context.Organizations.AsNoTracking()
                            .SingleOrDefaultAsync(x => x.OrganizationId.Eq(ap.OrganizationId.Value));
                if (organization == null)
                {
                    test = false;
                    errors.Add("указанная организация не существует");
                }

                var organizationProfile =
                    await
                        _context.OrganizationProfiles.AsNoTracking()
                            .SingleOrDefaultAsync(x => x.OrganizationProfileId.Eq(organization.OrganizationId));
                if (organizationProfile == null)
                {
                    test = false;
                    errors.Add("указанный профиль организации не существует");
                }
                var purchaseProfile =
                    await
                        _context.PurchaseProfiles.AsNoTracking()
                            .SingleOrDefaultAsync(x => x.AuctionProfileId.Eq(id));
                if (purchaseProfile == null)
                {
                    test = false;
                    errors.Add("указанный профиль закупок не существует");
                }

                var stock =
                    await
                        _context.Stocks.AsNoTracking()
                            .SingleOrDefaultAsync(x => x.StockId.Eq(organization.OrganizationId));
                if (stock == null)
                {
                    test = false;
                    errors.Add("указанный в организации склад не существует");
                }

                if (test)
                {

                    var doc = new AuctionNotificationViewModelStr
                    {
                        CustomerName = Helper.Safe(organizationProfile.RegistredName),
                        CustomerAddres =
                            Helper.Safe(
                                $"{organizationProfile.Street} {organizationProfile.Apartment} {organizationProfile.Office}"),
                        CustomerEmail = Helper.Safe(organizationProfile.Email),
                        CustomerPhone = Helper.Safe(organizationProfile.Phone),
                        CustomerResponsible = string.Empty, //нет привязки
                        CustomerPerformer = string.Empty, //нет привязки

                        PurchaseName = Helper.Safe(purchaseProfile.ProductName),
                        PurchaseCount = Helper.Safe(purchaseProfile.ProductCount),
                        PurchaseAddres = Helper.Safe(stock.LoadingAddress),
                        PurchaseDateDelivery = Helper.Safe(purchaseProfile.SupplyDate),
                        PurchasePrice = Helper.Safe(Math.PI), //нет привязки
                        SourceOfFinancing = Helper.Safe(organizationProfile.SourceOfFinancing),
                        RequirementsForContent =
                            "Указано в документации о проведении электронного аукциона в электронной форме",
                        PurchasesIdentificationCode = Helper.Safe(DateTime.Now), //нет привязки
                        LimitationParticipation = "Отсутствие в реестре недобросовестных поставщиков" +
                                                  " сведений об участниках размещения " +
                                                  "заказа, в том числе информации об " +
                                                  "учредителях, о членах коллегиального" +
                                                  " исполнительного органа, лице, исполняющем" +
                                                  " функции единоличного исполнительного органа" +
                                                  " участника закупки",

                        MethodDeterminingSupplier = "Электронный аукцион",
                        DeadlineForSubmission = Helper.Safe(DateTime.Now), //нет привязки
                        SizeCalculated = Helper.Safe(ap.ApplicationCoveragePercentage), //нет привязки
                        Summ = string.Empty, //нет привязки
                        ProcedureGranting = string.Empty, //нет привязки
                        AccountDetails = string.Empty, //нет привязки
                        Size = Helper.Safe(ap.ApplicationCoveragePercentage),
                        ProcedureGrantingContract = "Обеспечение исполнения контракта предоставляется" +
                                                    "участником электронного аукциона,с которым заключается " +
                                                    "контракт,путем внесения денежных средств на расчетный " +
                                                    "счет Заказчика,указанный в документации об электронном " +
                                                    "аукционе или безотзывной банковской гарантией,выданной" +
                                                    " банком,включенным в перечень,указанный в пункте 6.3.5" +
                                                    " части II \"ИНСТРУКЦИЯ УЧАСТНИКАМ АУКЦИОНА В ЭЛЕКТРОННОЙ " +
                                                    "ФОРМЕ\".Способ обеспечения исполнения контракта определяется" +
                                                    " участником электронного аукциона самостоятельно.Участник " +
                                                    "электронного аукциона,с которым заключается контракт,размещает" +
                                                    " в ЕИС(на официальном сайте) документ,подтверждающий " +
                                                    "предоставление обеспечения исполнения контракта подписанный усиленной " +
                                                    "электронной подписью лица,имеющего право действовать от имени участника электронного " +
                                                    "аукциона с которым заключается контракт вместе с проектом контракта,подписанным указанным лицом." +
                                                    "В случае,если участником электронного аукциона,с которым заключается контракт,является " +
                                                    "государственное или муниципальное казенное учреждение,то предоставление" +
                                                    " обеспечения контракта не требуется.",

                        DateGrantingContract = "Документы, подтверждающие " +
                                               "предоставление обеспечения исполнения контракта (платежное поручение," +
                                               " подтверждающее перечисление денежных средств в качестве обеспечения исполнения" +
                                               " Контракта с отметкой банка, или заверенная банком копия этого платежного поручения " +
                                               "либо включенная в реестр банковских гарантий безотзывная банковская гарантия в размере," +
                                               " который предусмотрен пунктом 7«ИНФОРМАЦИОННАЯ КАРТА АУКЦИОНА В ЭЛЕКТРОННОЙ ФОРМЕ," +
                                               "должны быть предоставлены заказчику в срок,указанный в пункте 7 \"ИНФОРМАЦИОННАЯ " +
                                               "КАРТА АУКЦИОНА В ЭЛЕКТРОННОЙ ФОРМЕ\", одновременно с проектом контракта," +
                                               " подписанным усиленной электронной подписью лица, имеющего право действовать" +
                                               " от имени участника электронного аукциона с которым заключается контракт.",
                        RequirementsFor = "1.В рамках предоставления обеспечения контракта должны быть обеспечены" +
                                          " обязательства исполнителя по контракту по возмещению убытков заказчика, " +
                                          "причиненных неисполнением или ненадлежащим исполнением обязательств по контракту, " +
                                          "а также обязанность выплаты неустойки, " +
                                          "предусмотренной контрактом. " +
                                          "2.Если при проведении электронного аукциона начальная(максимальная)" +
                                          " цена контракта,указанная в пункте 2 \"ИНФОРМАЦИОННАЯ КАРТА АУКЦИОНА" +
                                          " В ЭЛЕКТРОННОЙ ФОРМЕ\" составляет более чем пятнадцать миллионов рублей" +
                                          " и участником электронного аукциона, с которым заключается контракт, " +
                                          "предложена цена контракта, которая на двадцать пять и более процентов" +
                                          " ниже начальной (максимальной) цены контракта, то обеспечение исполнения" +
                                          " контракта должно быть представлено в размере, превышающем в полтора раза " +
                                          "размер обеспечения исполнения контракта, указанный в пункте 7" +
                                          " \"ИНФОРМАЦИОННАЯ КАРТА АУКЦИОНА В ЭЛЕКТРОННОЙ ФОРМЕ\", но не" +
                                          " менее чем в размере аванса (если контрактом предусмотрена выплата " +
                                          "аванса).3.Если при проведении электронного аукциона" +
                                          " начальная(максимальная) цена контракта, указанная в пункте 2 " +
                                          "\"ИНФОРМАЦИОННАЯ КАРТА АУКЦИОНА В ЭЛЕКТРОННОЙ ФОРМЕ\" составляет" +
                                          " пятнадцать и менее миллионов рублей и участником электронного " +
                                          "аукциона, с которым заключается контракт, предложена цена контракта, " +
                                          "которая на двадцать пять и более процентов ниже начальной (максимальной)" +
                                          " цены контракта, то в случае если участником электронного аукциона при" +
                                          " направлении заказчику подписанного проекта контракта не предоставляется " +
                                          "информация, подтверждающая его добросовестность в соответствии \"ИНСТРУКЦИЯ УЧАСТНИКАМ " +
                                          "АУКЦИОНА В ЭЛЕКТРОННОЙ ФОРМЕ\", обеспечение исполнения контракта должно быть представлено в размере," +
                                          " в полтора раза превышающем размер обеспечения исполнения Контракта, указанный в пункте 7 \"ИНФОРМАЦИОННАЯ КАРТА АУКЦИОНА" +
                                          " В ЭЛЕКТРОННОЙ ФОРМЕ\", но не менее чем в размере аванса (если контрактом предусмотрена выплата аванса).",
                        BankAccountTransferPerformanceContract = "Получатель платежа – ФКУ ИК-4 УФСИН России " +
                                                                 "по Оренбургской области, 460019," +
                                                                 " г.Оренбург,ул.Техническая,2,ИНН 5609031170,КПП " +
                                                                 "560901001,УФК по Оренбургской области(ОФК 08, л / " +
                                                                 "с 05531140900) р / счет № 40302810800001000011" +
                                                                 " Отделение  г.Оренбург,БИК 045354001)",

                        TermsBankGuarantePerformanceContract = "Банковская гарантия должна быть безотзывной " +
                                                               "и содержать:1) сумму банковской гарантии, " +
                                                               "указанную в пункте 7 части III \"ИНФОРМАЦИОННАЯ" +
                                                               " КАРТА АУКЦИОНА В ЭЛЕКТРОННОЙ ФОРМЕ\" и подлежащую " +
                                                               "уплате гарантом заказчику при ненадлежащем исполнении" +
                                                               " принципала обязательств, указанных в пп. 2 " +
                                                               "настоящего пункта части III \"ИНФОРМАЦИОННАЯ КАРТА" +
                                                               " АУКЦИОНА В ЭЛЕКТРОННОЙ ФОРМЕ\";2) обязательства" +
                                                               " принципала, надлежащее исполнение которых " +
                                                               "обеспечивается банковской гарантией;3) обязанность " +
                                                               "гаранта уплатить заказчику неустойку в размере 0,1" +
                                                               " процента денежной суммы, подлежащей уплате," +
                                                               " за каждый календарный день просрочки;4) условие, " +
                                                               "согласно которому исполнением обязательств гаранта " +
                                                               "по банковской гарантии является фактическое" +
                                                               " поступление денежных сумм на счет, на котором в" +
                                                               " соответствии с законодательством Российской" +
                                                               " Федерации учитываются операции со средствами," +
                                                               " поступающими заказчику;5) срок действия банковской " +
                                                               "гарантии, указанный в пункте 44 части" +
                                                               " III \"ИНФОРМАЦИОННАЯ КАРТА АУКЦИОНА В" +
                                                               " ЭЛЕКТРОННОЙ ФОРМЕ\";6) перечень документов, " +
                                                               "предоставляемых заказчиком банку одновременно " +
                                                               "с требованием об осуществлении уплаты денежной " +
                                                               "суммы по банковской гарантии в соответствии с " +
                                                               "Постановлением Правительства РФ от 8 ноября " +
                                                               "2013 г.N 1005 «О банковских гарантиях, используемых" +
                                                               " для целей федерального закона \"О контрактной" +
                                                               " системе в сфере закупок товаров, работ, услуг для" +
                                                               " обеспечения государственных и муниципальных нужд\"" +
                                                               " :расчет суммы, включаемой в требование по банковской" +
                                                               " гарантии; платежное поручение, подтверждающее перечисление " +
                                                               "бенефициаром аванса принципалу, с отметкой банка бенефициара " +
                                                               "либо органа Федерального казначейства об исполнении(если выплата" +
                                                               " аванса предусмотрена контрактом, а требование по банковской гарантии" +
                                                               " предъявлено в случае ненадлежащего исполнения принципалом обязательств " +
                                                               "по возврату аванса);документ, подтверждающий факт наступления гарантийного" +
                                                               " случая в соответствии с условиями контракта (если требование по банковской" +
                                                               " гарантии предъявлено в случае ненадлежащего исполнения принципалом обязательств" +
                                                               " в период действия гарантийного срока);документ, подтверждающий полномочия единоличного " +
                                                               "исполнительного органа(или иного уполномоченного лица), подписавшего требование по банковской " +
                                                               "гарантии(решение об избрании, приказ о назначении, доверенность);7) отлагательное условие, предусматривающее" +
                                                               " заключение договора предоставления банковской гарантии по обязательствам принципала," +
                                                               " возникшим из контракта при его заключении.Заказчик рассматривает поступившую" +
                                                               " в качестве обеспечения исполнения контракта банковскую гарантию в срок, не превышающий трех " +
                                                               "рабочих дней со дня ее поступления.Основанием для отказа в принятии банковской " +
                                                               "гарантии заказчиком является:1) отсутствие информации о банковской гарантии " +
                                                               "в реестре банковских гарантий 2) несоответствие банковской гарантии условиям," +
                                                               " указанным в настоящем пункте части III \"ИНФОРМАЦИОННАЯ КАРТА АУКЦИОНА В" +
                                                               " ЭЛЕКТРОННОЙ \"ФОРМЕ\"; 3) несоответствие банковской гарантии требованиям," +
                                                               " содержащимся в извещении о проведении электронного аукциона, настоящей документации" +
                                                               " об электронном аукционе.В случае отказа в принятии банковской гарантии заказчик " +
                                                               "в срок, не превышающий трех рабочих дней со дня ее поступления, информирует в письменной" +
                                                               " форме или в форме электронного документа об этом лицо, предоставившее банковскую гарантию, " +
                                                               "с указанием причин, послуживших основанием для отказа.",
                        ValidityBankGuarantee =
                            "Срок действия банковской гарантии должен превышать срок действия контракта не менее чем на один месяц",

                        RequirementsParticipants =
                            "1) соответствие требованиям, установленным в соответствии с законодательством Российской Федерации к лицам, осуществляющим поставку товара, выполнение работы, оказание услуги, " +
                            "являющихся объектом закупки;2) непроведение ликвидации участника закупки -юридического лица и отсутствие решения арбитражного суда о признании участника закупки -юридического лица" +
                            " или индивидуального предпринимателя несостоятельным(банкротом) и об открытии конкурсного производства;3) неприостановление деятельности участника закупки в порядке," +
                            " установленном Кодексом Российской Федерации об административных правонарушениях, на дату подачи заявки на участие в закупке;4) отсутствие у участника закупки недоимки" +
                            " по налогам, сборам, задолженности по иным обязательным платежам в бюджеты бюджетной системы Российской Федерации(за исключением сумм, на которые предоставлены отсрочка," +
                            " рассрочка, инвестиционный налоговый кредит в соответствии с законодательством Российской Федерации о налогах и сборах, которые реструктурированы в " +
                            "соответствии с законодательством Российской Федерации, по которым имеется вступившее в законную силу решение суда о признании обязанности заявителя" +
                            " по уплате этих сумм исполненной или которые признаны безнадежными к взысканию в соответствии с законодательством Российской Федерации о налогах и сборах) за прошедший " +
                            "календарный год, размер которых превышает двадцать пять процентов балансовой стоимости" +
                            " активов участника закупки, по данным бухгалтерской отчетности за последний отчетный период." +
                            "Участник закупки считается соответствующим установленному требованию в случае, если им в установленном порядке подано заявление об обжаловании указанных " +
                            "недоимки, задолженности и решение по такому заявлению на дату рассмотрения заявки на участие в определении " +
                            "поставщика (подрядчика, исполнителя) не принято;5) отсутствие у участника закупки" +
                            " -физического лица либо у руководителя, членов коллегиального исполнительного органа или главного бухгалтера юридического лица -участника закупки судимости " +
                            "за преступления в сфере экономики(за исключением лиц, у которых такая судимость погашена или снята), а также неприменение в отношении указанных физических лиц " +
                            "наказания в виде лишения права занимать определенные должности или заниматься определенной деятельностью, которые связаны с поставкой товара, выполнением работы," +
                            " оказанием услуги, являющихся объектом осуществляемой закупки, и административного наказания в виде дисквалификации;6) отсутствие между участником" +
                            " закупки и заказчиком конфликта интересов, под которым понимаются случаи, при которых руководитель заказчика, член комиссии по осуществлению закупок, руководитель " +
                            "контрактной службы заказчика, контрактный управляющий состоят в браке с физическими лицами, являющимися выгодоприобретателями, единоличным исполнительным органом" +
                            " хозяйственного общества(директором, генеральным директором, управляющим, президентом и другими), членами коллегиального исполнительного органа хозяйственного общества, " +
                            "руководителем (директором, генеральным директором) учреждения или унитарного предприятия либо иными органами управления юридических лиц - участников закупки, с физическими" +
                            " лицами, в том числе зарегистрированными в качестве индивидуального предпринимателя, -участниками закупки либо являются близкими родственниками(родственниками по прямой " +
                            "восходящей и нисходящей линии(родителями и детьми, дедушкой, бабушкой и внуками), полнородными и неполнородными(имеющими общих отца или мать) братьями и сестрами), " +
                            "усыновителями или усыновленными указанных физических лиц.Под   выгодоприобретателями для целей настоящей статьи понимаются физические лица, владеющие напрямую или" +
                            " косвенно(через юридическое лицо или через несколько юридических лиц) более чем десятью процентами голосующих акций хозяйственного общества либо долей, превышающей десять" +
                            " процентов в уставном капитале хозяйственного общества.7)Отсутствие в реестре недобросовестных поставщиков сведений" +
                            " об участниках размещения заказа, в том числе информации об учредителях, о членах коллегиального исполнительного органа," +
                            " лице, исполняющем функции единоличного исполнительного органа участника закупки - юридического лица.8) участник закупки не является" +
                            " офшорной компанией.",
                        EmailSite = Helper.Safe(ap.ElectronicTradingPlatform),
                        RequirementsCompositionParts =
                            "Заявка на участие в электронном аукционе должна состоять из двух частей:Первая" +
                            " часть заявки должна содержать следующие сведения:1)	согласие участника такого " +
                            "аукциона на поставку товара(согласно разделу 1.6 документации);2) конкретные показатели," +
                            " соответствующие значениям, установленным документацией о таком аукционе, и указание на товарный " +
                            "знак(его словесное обозначение), знак обслуживания, фирменное наименование, патенты, полезные модели," +
                            " промышленные образцы, наименование места происхождения товара или наименование производителя предлагаемого" +
                            " для поставки товара при условии отсутствия в данной документации указания на товарный знак, знак обслуживания," +
                            " фирменное наименование, патенты, полезные модели, промышленные образцы, наименование места происхождения товара" +
                            " или наименование производителя;Вторая часть заявки должна " +
                            "содержать следующие документы и сведения:1) наименование, фирменное" +
                            " наименование (при наличии), место нахождения, почтовый адрес(для юридического лица)," +
                            " фамилия, имя, отчество(при наличии), паспортные данные, место жительства(для физического лица), " +
                            "номер контактного телефона, идентификационный номер налогоплательщика участника такого аукциона или в" +
                            " соответствии с законодательством соответствующего иностранного государства аналог идентификационного " +
                            "номера налогоплательщика участника такого аукциона(для иностранного лица), идентификационный номер налогоплательщика" +
                            "(при наличии) учредителей, членов коллегиального исполнительного органа, лица, исполняющего функции единоличного исполнительного" +
                            " органа участника такого аукциона;2) документы, подтверждающие соответствие участника" +
                            " такого аукциона требованиям, установленным пунктом 1 части 1 и частью 2 статьи 31(при наличии таких) требований" +
                            " настоящего Федерального закона или копии этих документов, а также декларация о соответствии участника такого аукциона" +
                            " требованиям, установленным пунктами 3 - 9 части 1 статьи 31 настоящего Федерального закона.3) копии документов, подтверждающих" +
                            " соответствие работы требованиям, установленным в соответствии с законодательством Российской Федерации, в случае, если в соответствии" +
                            " с законодательством Российской Федерации установлены требования к работе и представление указанных документов предусмотрено документацией" +
                            " об электронном аукционе.При этом не допускается требовать представление указанных документов, если в соответствии с законодательством" +
                            " Российской Федерации они передаются вместе с товаром;4) решение об одобрении или о совершении крупной" +
                            " сделки либо копия данного решения в случае, если требование о необходимости наличия данного решения для" +
                            " совершения крупной сделки установлено федеральными законами и иными нормативными правовыми" +
                            " актами Российской Федерации и(или) учредительными документами юридического лица и" +
                            " для участника такого аукциона заключаемый контракт" +
                            " или предоставление обеспечения заявки на участие" +
                            " в электронном аукционе, обеспечения исполнения контракта" +
                            " является крупной сделкой.",
                        DateEndFirst = Helper.Safe(ap.DateOfReview),
                        DateAuction = Helper.Safe(ap.AuctionDate),
                        BenefitsParticipants = "Не установлены.",
                        TermsAdmissionGoodsForeignCountry = "Не установлены."
                    };

                    return Json(doc);
                }
                return Json(errors);
            }
            return NotFound("404");
        }

        public async Task<IActionResult> GetInformationCardDocument(Guid id)
        {

            var errors = new List<string>();

            var ap = await _context.AuctionProfiles.AsNoTracking().SingleOrDefaultAsync(x => x.AuctionProfileId == id);

            var test = ap != null && ap.OrganizationId.HasValue;

            if (!test)
            {
                errors.Add("такого аукциона не сущесвует, либо вы не указали организацию");

            }
            else
            {
                var organization =
                    await
                        _context.Organizations.AsNoTracking()
                            .SingleOrDefaultAsync(x => x.OrganizationId.Eq(ap.OrganizationId.Value));
                if (organization == null)
                {
                    test = false;
                    errors.Add("указанная организация не существует");
                }

                var organizationProfile =
                    await
                        _context.OrganizationProfiles.AsNoTracking()
                            .SingleOrDefaultAsync(x => x.OrganizationProfileId.Eq(organization.OrganizationId));
                if (organizationProfile == null)
                {
                    test = false;
                    errors.Add("указанный профиль организации не существует");
                }
                var purchaseProfile =
                    await
                        _context.PurchaseProfiles.AsNoTracking()
                            .SingleOrDefaultAsync(x => x.AuctionProfileId.Eq(id));
                if (purchaseProfile == null)
                {
                    test = false;
                    errors.Add("указанный профиль закупок не существует");
                }

                if (test)
                {

                    var doc = new InformationAuctionCard()
                    {
                        CustomerName = Helper.Safe(organizationProfile.RegistredName),
                        CustomerAddres =
                            Helper.Safe(
                                $"{organizationProfile.Street} {organizationProfile.Apartment} {organizationProfile.Office}"),
                        CustomerEmail = Helper.Safe(organizationProfile.Email),
                        CustomerPhone = Helper.Safe(organizationProfile.Phone),
                        CustomerResponsible = string.Empty, //нет привязки

                        PurchaseName = Helper.Safe(purchaseProfile.ProductName),
                    };
                    return Json(doc);
                }
                return Json(errors);
            }
            return NotFound("");
        }

        // GET: AuctionProfile
        [HttpGet]
        public async Task<IActionResult> GetMyList()
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            @ViewBag.Id = Guid.NewGuid();

            var existsAuctionProfiles = await _context.AuctionProfiles.AnyAsync();
            var auctionViews = new List<AuctionView>();
            
            if (existsAuctionProfiles)
            {
                var organizations =
                    _context.Organizations.Where(x =>
                        x.OrganizationToManagers.Any(
                            y => y.ApplicationUserId.Equals(userId, StringComparison.OrdinalIgnoreCase))
                        ||
                        x.Holders.Any(
                            y => y.ApplicationUserId.Equals(userId, StringComparison.OrdinalIgnoreCase))
                        ||
                        x.Signatories.Any(
                            y => y.ApplicationUserId.Equals(userId, StringComparison.OrdinalIgnoreCase))
                        ||
                        x.Responsibles.Any(
                            y => y.ApplicationUserId.Equals(userId, StringComparison.OrdinalIgnoreCase))
                        ).ToList();

                 auctionViews =
                    _context.AuctionProfiles.Include(x => x.Organization)
                        .Where(x => organizations.Any(y => y.OrganizationId.Eq(x.OrganizationId.Value)))
                        .ToList()
                        .Select(Mapper.Map<AuctionProfile, AuctionView>).ToList();
            }
            return View(auctionViews);
        }

        //[HttpGet]
        //public IActionResult GetCalendar()
        //{
        //    var aucttion = _context.AuctionProfile.First(x => x.AuctionDateStart.HasValue && x.AuctionDateEnd.HasValue);
        //    _calendar.AddOrUpdate(aucttion);

        //    return View();
        //}

        //хз безэтого не работает oauth 
        [HttpGet("/authorize/{code}")]
        public IActionResult GetAuth(string code)
        {
            return View();
        }


    }
}