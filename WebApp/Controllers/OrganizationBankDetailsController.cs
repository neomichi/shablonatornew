using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApp.Models;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Controllers
{
    public class OrganizationBankDetailsController : Controller
    {
        private ApplicationDbContext _context;

        public OrganizationBankDetailsController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: OrganizationBankDetails
        public async Task<IActionResult> Index()
        {
            return View(await _context.OrganizationBankDetails.ToListAsync());
        }

        // GET: OrganizationBankDetails/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            OrganizationBankDetails organizationBankDetails = await _context.OrganizationBankDetails.SingleAsync(m => m.OrganizationBankDetailsId == id);
            if (organizationBankDetails == null)
            {
                return NotFound();
            }

            return View(organizationBankDetails);
        }

        // GET: OrganizationBankDetails/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: OrganizationBankDetails/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(OrganizationBankDetails organizationBankDetails)
        {
            if (ModelState.IsValid)
            {
                organizationBankDetails.OrganizationBankDetailsId = Guid.NewGuid();
                _context.OrganizationBankDetails.Add(organizationBankDetails);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(organizationBankDetails);
        }

        // GET: OrganizationBankDetails/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            OrganizationBankDetails organizationBankDetails = await _context.OrganizationBankDetails.FirstOrDefaultAsync(m => m.OrganizationBankDetailsId == id);
            if (organizationBankDetails == null)
            {
                organizationBankDetails = new OrganizationBankDetails { OrganizationBankDetailsId = (Guid)id };
                _context.OrganizationBankDetails.Add(organizationBankDetails);
                await _context.SaveChangesAsync();
            }
            return View(organizationBankDetails);
        }

        // POST: OrganizationBankDetails/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(OrganizationBankDetails organizationBankDetails)
        {
            if (ModelState.IsValid)
            {
                _context.Update(organizationBankDetails);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index", "OrganizationSummary", new { id = organizationBankDetails.OrganizationBankDetailsId });
            }
            return View(organizationBankDetails);
        }

        // GET: OrganizationBankDetails/Delete/5
        [ActionName("Delete")]
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            OrganizationBankDetails organizationBankDetails = await _context.OrganizationBankDetails.SingleAsync(m => m.OrganizationBankDetailsId == id);
            if (organizationBankDetails == null)
            {
                return NotFound();
            }

            return View(organizationBankDetails);
        }

        // POST: OrganizationBankDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            OrganizationBankDetails organizationBankDetails = await _context.OrganizationBankDetails.SingleAsync(m => m.OrganizationBankDetailsId == id);
            _context.OrganizationBankDetails.Remove(organizationBankDetails);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }
    }
}
