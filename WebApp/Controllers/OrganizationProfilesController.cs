using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApp.Models;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Controllers
{
    public class OrganizationProfilesController : Controller
    {
        private ApplicationDbContext _context;

        public OrganizationProfilesController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: OrganizationProfiles
        public async Task<IActionResult> Index()
        {
            return View(await _context.OrganizationProfiles.ToListAsync());
        }

        // GET: OrganizationProfiles/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            OrganizationProfile organizationProfile = await _context.OrganizationProfiles.SingleAsync(m => m.OrganizationProfileId == id);
            if (organizationProfile == null)
            {
                return NotFound();
            }

            return View(organizationProfile);
        }

        // GET: OrganizationProfiles/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: OrganizationProfiles/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(OrganizationProfile organizationProfile)
        {
            if (ModelState.IsValid)
            {
                organizationProfile.OrganizationProfileId = Guid.NewGuid();
                _context.OrganizationProfiles.Add(organizationProfile);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(organizationProfile);
        }

        // GET: OrganizationProfiles/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            OrganizationProfile organizationProfile = await _context.OrganizationProfiles.FirstOrDefaultAsync(m => m.OrganizationProfileId == id);
            if (organizationProfile == null)
            {
                organizationProfile = new OrganizationProfile { OrganizationProfileId = (Guid)id };
                _context.OrganizationProfiles.Add(organizationProfile);
                await _context.SaveChangesAsync();
            }
            return View(organizationProfile);
        }

        // POST: OrganizationProfiles/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(OrganizationProfile organizationProfile)
        {
            if (ModelState.IsValid)
            {
                _context.Update(organizationProfile);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index", "OrganizationSummary", new {id = organizationProfile.OrganizationProfileId});
            }
            return View(organizationProfile);
        }

        // GET: OrganizationProfiles/Delete/5
        [ActionName("Delete")]
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            OrganizationProfile organizationProfile = await _context.OrganizationProfiles.SingleAsync(m => m.OrganizationProfileId == id);
            if (organizationProfile == null)
            {
                return NotFound();
            }

            return View(organizationProfile);
        }

        // POST: OrganizationProfiles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            OrganizationProfile organizationProfile = await _context.OrganizationProfiles.SingleAsync(m => m.OrganizationProfileId == id);
            _context.OrganizationProfiles.Remove(organizationProfile);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }
    }
}
