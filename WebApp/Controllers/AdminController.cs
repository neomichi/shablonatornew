﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using WebApp.Models;
using WebApp.ViewModels.User;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Controllers
{
    public class AdminController : Controller
    {
        private readonly ApplicationDbContext _context;
        public AdminController(ApplicationDbContext context)
        {
            _context = context;
        }


        [HttpGet]
        public IActionResult UserList()
        {
            var links = new List<string>()
            {
                Url.Action("EditUser", "Admin", new {Id = 0}),
                Url.Action("DeleteUser", "Admin", new {Id = 0}),
            };
            return View(links);
        }

        [HttpGet]
        public async Task<IActionResult> GetUserList()
        {
            var UserWithRoles = new List<UserWithRole>();
            using (var userStore = new UserStore<ApplicationUser>(_context))
            {
                var users = await userStore.Users.ToListAsync();
                foreach (var user in users)
                {
                    var roleList = await userStore.GetRolesAsync(user);
                    var roles = string.Join(" ", roleList);
                    var userWithRole=new UserWithRole();
                    ;
                    userWithRole.Email = string.IsNullOrWhiteSpace(user.Email) ? "не установлено" : user.Email;
                    userWithRole.Picture = string.IsNullOrWhiteSpace(user.Picture) ? "не установлено" : user.Picture;
                    userWithRole.FullName = string.IsNullOrWhiteSpace(user.FullName)?"не установлено":user.FullName;
                    userWithRole.Id = user.Id;
                    userWithRole.PhoneNumber= string.IsNullOrWhiteSpace(user.PhoneNumber) ? "не установлено" : user.PhoneNumber;
              
                 //   userWithRole = Mapper.Map<ApplicationUser, UserWithRole>(user);
                
                    userWithRole.Roles = string.IsNullOrWhiteSpace(roles) ? "Не назначено" : roles;
                    userWithRole.RemoveLink = Url.Action("DeleteUser", "Admin",new {Id = user.Id});
                     //, onclick = "return confirm('Вы уверены? ');"
                    userWithRole.EditRoleLink = Url.Action("EditUser", "Admin", new { Id = user.Id });

                    UserWithRoles.Add(userWithRole);

                }
            }
            return Json(new { data = UserWithRoles });
        }


        [HttpGet]
        public async Task<IActionResult> EditUser(string Id)
        {
            var user =
                await
                    _context.Users.AsNoTracking().Include(x => x.Roles)
                        .SingleOrDefaultAsync(x => x.Id.Equals(Id, StringComparison.OrdinalIgnoreCase));
            if (user != null)
            {
                var userWithRole = Mapper.Map<ApplicationUser, UserWithRole>(user);
                var userroles = user.Roles.Select(x => x.RoleId).ToList();
                var allRoles = _context.Roles.AsNoTracking().Select(x => new SelectListItem
                {
                    Value = x.Name,
                    Text = x.Id,
                    Selected = userroles.Any(y => y.Equals(x.Id))
                }).ToList();
                userWithRole.AllRoles = allRoles;
                return View(userWithRole);
            }
            return NotFound("404");
        }

        [HttpPost]
        public async Task<IActionResult> EditUser(UserWithRole userWithRole)
        {

            var roles = Request.Form.Where(x => Code.Constant.Roles.Contains(x.Key))
                .ToDictionary(x => x.Key, x => x.Value);
            
            using (var userStore = new UserStore<ApplicationUser>(_context))
            {
                var user = await userStore.FindByIdAsync(userWithRole.Id);
                if (user != null)
                {
                    foreach (var role in Code.Constant.Roles)
                    {
                        var userIsInRoleAsync = await userStore.IsInRoleAsync(user, role);
                        
                        if (roles.Keys.Contains(role))
                        {
                            if (!userIsInRoleAsync)
                            {
                                await userStore.AddToRoleAsync(user, role);
                            }
                        }
                        else
                        {
                            if (userIsInRoleAsync)
                            {
                                await userStore.RemoveFromRoleAsync(user, role);
                            }
                        }
                    }
                    _context.SaveChanges();
                }
            }
            return RedirectToAction("EditUser", new { Id = userWithRole.Id });


        }


        public async Task<IActionResult> DeleteUser(string Id)
        {
            using (_context)
            {
                var user = await _context.Users.SingleOrDefaultAsync(x => x.Id.Equals(Id, StringComparison.OrdinalIgnoreCase));
                _context.Remove(user);
               await _context.SaveChangesAsync();
            }
            return RedirectToAction("UserList");
        }

        [Route("/authorize")]
        public IActionResult Test(string code)
        {
            return View();
        }
    }
}