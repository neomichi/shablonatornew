using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApp.Models;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Controllers
{
    public class OrganizationProvisionRecipientBankDetailsController : Controller
    {
        private ApplicationDbContext _context;

        public OrganizationProvisionRecipientBankDetailsController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: OrganizationProvisionRecipientBankDetails
        public async Task<IActionResult> Index()
        {
            return View(await _context.OrganizationProvisionRecipientBankDetails.ToListAsync());
        }

        // GET: OrganizationProvisionRecipientBankDetails/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            OrganizationProvisionRecipientBankDetails organizationProvisionRecipientBankDetails = await _context.OrganizationProvisionRecipientBankDetails.SingleAsync(m => m.OrganizationProvisionRecipientBankDetailsId == id);
            if (organizationProvisionRecipientBankDetails == null)
            {
                return NotFound();
            }

            return View(organizationProvisionRecipientBankDetails);
        }

        // GET: OrganizationProvisionRecipientBankDetails/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: OrganizationProvisionRecipientBankDetails/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(OrganizationProvisionRecipientBankDetails organizationProvisionRecipientBankDetails)
        {
            if (ModelState.IsValid)
            {
                organizationProvisionRecipientBankDetails.OrganizationProvisionRecipientBankDetailsId = Guid.NewGuid();
                _context.OrganizationProvisionRecipientBankDetails.Add(organizationProvisionRecipientBankDetails);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(organizationProvisionRecipientBankDetails);
        }

        // GET: OrganizationProvisionRecipientBankDetails/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            OrganizationProvisionRecipientBankDetails organizationProvisionRecipientBankDetails = await _context.OrganizationProvisionRecipientBankDetails.FirstOrDefaultAsync(m => m.OrganizationProvisionRecipientBankDetailsId == id);
            if (organizationProvisionRecipientBankDetails == null)
            {
                organizationProvisionRecipientBankDetails = new OrganizationProvisionRecipientBankDetails { OrganizationProvisionRecipientBankDetailsId = (Guid)id };
                _context.OrganizationProvisionRecipientBankDetails.Add(organizationProvisionRecipientBankDetails);
                await _context.SaveChangesAsync();
            }
            return View(organizationProvisionRecipientBankDetails);
        }

        // POST: OrganizationProvisionRecipientBankDetails/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(OrganizationProvisionRecipientBankDetails organizationProvisionRecipientBankDetails)
        {
            if (ModelState.IsValid)
            {
                _context.Update(organizationProvisionRecipientBankDetails);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index", "OrganizationSummary", new { id = organizationProvisionRecipientBankDetails.OrganizationProvisionRecipientBankDetailsId });
            }
            return View(organizationProvisionRecipientBankDetails);
        }

        // GET: OrganizationProvisionRecipientBankDetails/Delete/5
        [ActionName("Delete")]
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            OrganizationProvisionRecipientBankDetails organizationProvisionRecipientBankDetails = await _context.OrganizationProvisionRecipientBankDetails.SingleAsync(m => m.OrganizationProvisionRecipientBankDetailsId == id);
            if (organizationProvisionRecipientBankDetails == null)
            {
                return NotFound();
            }

            return View(organizationProvisionRecipientBankDetails);
        }

        // POST: OrganizationProvisionRecipientBankDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            OrganizationProvisionRecipientBankDetails organizationProvisionRecipientBankDetails = await _context.OrganizationProvisionRecipientBankDetails.SingleAsync(m => m.OrganizationProvisionRecipientBankDetailsId == id);
            _context.OrganizationProvisionRecipientBankDetails.Remove(organizationProvisionRecipientBankDetails);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }
    }
}
