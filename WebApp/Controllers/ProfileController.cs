using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.PlatformAbstractions;
using WebApp.Models;
using WebApp.ViewModels.Profile;

namespace WebApp.Controllers
{
    public class ProfileController : Controller
    {
        private ApplicationDbContext _context;
     
        private readonly IHostingEnvironment _hostingEnvironment;

        public ProfileController(ApplicationDbContext context,   IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
            _context = context;
        }

        public IActionResult Index()
        {
            var user = _context.Users.First(x => x.UserName == User.Identity.Name);
            var profile = Mapper.Map<ApplicationUser, ProfileViewModel>(user);
            return View(profile);
        }

        public IActionResult Edit()
        {
            var user = _context.Users.First(x => x.UserName == User.Identity.Name);
            var profile = Mapper.Map<ApplicationUser, ProfileViewModel>(user);
            return View(profile);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(ProfileViewModel profile)
        {
            var newuser=new ApplicationUser();
            if (ModelState.IsValid)
            {
                var user = _context.Users.First(x => x.UserName == User.Identity.Name);
                user = Mapper.Map(profile, user);
                _context.Update(user);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(profile);
        }

        [HttpPost]
        public IActionResult UploadFiles()
        {
            var file = Request.Form.Files.FirstOrDefault();
            
            if (file != null
                && Regex.IsMatch(Code.Constant.ImageTypes, file.ContentType, RegexOptions.IgnoreCase)
                && file.Length < Code.Constant.ImageMaxSize)
            {
                var ext = Regex.Match(file.ContentDisposition, Code.Constant.RegexImageExt, RegexOptions.IgnoreCase);
                var filename = string.Format("{0}{1}", User.FindFirst(ClaimTypes.NameIdentifier).Value, ext.Groups[1].Value);
                var avatarDir = Path.Combine(_hostingEnvironment.WebRootPath, Code.Constant.UploadPath, Code.Constant.AvatarPath);
                var fullpath = Path.Combine(avatarDir, filename);
                if (!Directory.Exists(avatarDir))
                {
                    Directory.CreateDirectory(avatarDir);
                    if (System.IO.File.Exists(fullpath)) System.IO.File.Delete(fullpath);
                }
            
                using (var fileStream = new FileStream(Path.Combine(avatarDir, filename), FileMode.Create, FileAccess.Write))
                {
                    file.CopyTo(fileStream);
                }

                var webpath = String.Format("/{0}/{1}/{2}", Code.Constant.UploadPath, Code.Constant.AvatarPath, filename);
                return Json(webpath);

            }
            return NotFound("404");
        }



    }
}
