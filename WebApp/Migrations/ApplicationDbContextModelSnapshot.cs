﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using WebApp.Models;

namespace WebApp.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    partial class ApplicationDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.0-rtm-21431")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole", b =>
                {
                    b.Property<string>("Id");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("NormalizedName")
                        .HasAnnotation("MaxLength", 256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .HasName("RoleNameIndex");

                    b.ToTable("AspNetRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserToken<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("WebApp.Models.ApplicationUser", b =>
                {
                    b.Property<string>("Id");

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Email")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<string>("FullName");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("NormalizedUserName")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("Picture");

                    b.Property<string>("Position");

                    b.Property<string>("PowerOfAttorneyNumber");

                    b.Property<string>("SecurityStamp");

                    b.Property<string>("SocialProfile");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName")
                        .HasAnnotation("MaxLength", 256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex");

                    b.ToTable("AspNetUsers");
                });

            modelBuilder.Entity("WebApp.Models.AuctionProfile", b =>
                {
                    b.Property<Guid>("AuctionProfileId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("AdmissionFromForeigns");

                    b.Property<double?>("ApplicationCoveragePercentage");

                    b.Property<DateTime?>("AuctionDate");

                    b.Property<DateTime?>("AuctionDateEnd");

                    b.Property<DateTime?>("AuctionDateStart");

                    b.Property<string>("BeforeOfTheFederalLaw44");

                    b.Property<string>("BenefitsOfParticipating");

                    b.Property<DateTime?>("ClarificationDateEnd");

                    b.Property<DateTime?>("ClarificationDateStart");

                    b.Property<double?>("ContractCoveragePercentage");

                    b.Property<DateTime?>("DateOfReview");

                    b.Property<string>("DeadlineForSigning");

                    b.Property<string>("DocumentsAfterContract");

                    b.Property<string>("ElectronicTradingPlatform");

                    b.Property<Guid?>("OrganizationId");

                    b.Property<string>("TermsForCommercialCompanies");

                    b.Property<string>("TermsForGovermentCompanies");

                    b.Property<string>("TheRequirementInTheAbsenceOfTheFederalLaw44");

                    b.HasKey("AuctionProfileId");

                    b.HasIndex("OrganizationId");

                    b.ToTable("AuctionProfiles");
                });

            modelBuilder.Entity("WebApp.Models.Organization", b =>
                {
                    b.Property<Guid>("OrganizationId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.Property<Guid?>("OrganizationBankDetailsId");

                    b.Property<Guid?>("OrganizationProfileId");

                    b.Property<Guid?>("OrganizationProvisionRecipientBankDetailsId");

                    b.Property<Guid?>("StockId");

                    b.HasKey("OrganizationId");

                    b.HasIndex("OrganizationBankDetailsId");

                    b.HasIndex("OrganizationProfileId");

                    b.HasIndex("OrganizationProvisionRecipientBankDetailsId");

                    b.HasIndex("StockId");

                    b.ToTable("Organizations");
                });

            modelBuilder.Entity("WebApp.Models.OrganizationBankDetails", b =>
                {
                    b.Property<Guid>("OrganizationBankDetailsId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("BankAccount");

                    b.Property<string>("BankName");

                    b.Property<string>("Bik");

                    b.Property<string>("CorrespondentAccount");

                    b.Property<string>("Inn");

                    b.Property<string>("Kpp");

                    b.Property<string>("Ogrn");

                    b.Property<string>("Okpo");

                    b.Property<string>("Okved");

                    b.Property<string>("PersonalAccount");

                    b.Property<string>("RegistredOffice");

                    b.HasKey("OrganizationBankDetailsId");

                    b.ToTable("OrganizationBankDetails");
                });

            modelBuilder.Entity("WebApp.Models.OrganizationProfile", b =>
                {
                    b.Property<Guid>("OrganizationProfileId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Apartment");

                    b.Property<string>("ClosedForLunch");

                    b.Property<string>("Email");

                    b.Property<string>("Fax");

                    b.Property<string>("LinkLogo");

                    b.Property<string>("Location");

                    b.Property<string>("Name");

                    b.Property<string>("Office");

                    b.Property<string>("Phone");

                    b.Property<string>("PostIndex");

                    b.Property<string>("Region");

                    b.Property<string>("RegistredName");

                    b.Property<string>("Site");

                    b.Property<string>("SourceOfFinancing");

                    b.Property<string>("Street");

                    b.Property<string>("WorkingHours");

                    b.HasKey("OrganizationProfileId");

                    b.ToTable("OrganizationProfiles");
                });

            modelBuilder.Entity("WebApp.Models.OrganizationProvisionRecipientBankDetails", b =>
                {
                    b.Property<Guid>("OrganizationProvisionRecipientBankDetailsId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("BankAccount");

                    b.Property<string>("BankName");

                    b.Property<string>("Bik");

                    b.Property<string>("CorrespondentAccount");

                    b.Property<string>("Inn");

                    b.Property<string>("Kbk");

                    b.Property<string>("Kpp");

                    b.Property<string>("Ogrn");

                    b.Property<string>("Okpo");

                    b.Property<string>("Okved");

                    b.Property<string>("PersonalAccount");

                    b.Property<string>("PurposeOfPayment");

                    b.Property<string>("RegistredOffice");

                    b.HasKey("OrganizationProvisionRecipientBankDetailsId");

                    b.ToTable("OrganizationProvisionRecipientBankDetails");
                });

            modelBuilder.Entity("WebApp.Models.OrganizationToAdmin", b =>
                {
                    b.Property<Guid>("OrganizationId");

                    b.Property<string>("ApplicationUserId");

                    b.HasKey("OrganizationId", "ApplicationUserId");

                    b.HasIndex("ApplicationUserId");

                    b.HasIndex("OrganizationId");

                    b.ToTable("OrganizationToAdmins");
                });

            modelBuilder.Entity("WebApp.Models.OrganizationToManager", b =>
                {
                    b.Property<Guid>("OrganizationId");

                    b.Property<string>("ApplicationUserId");

                    b.HasKey("OrganizationId", "ApplicationUserId");

                    b.HasIndex("ApplicationUserId");

                    b.HasIndex("OrganizationId");

                    b.ToTable("OrganizationToManagers");
                });

            modelBuilder.Entity("WebApp.Models.OrganizationToResponsible", b =>
                {
                    b.Property<Guid>("OrganizationId");

                    b.Property<string>("ApplicationUserId");

                    b.HasKey("OrganizationId", "ApplicationUserId");

                    b.HasIndex("ApplicationUserId");

                    b.HasIndex("OrganizationId");

                    b.ToTable("OrganizationToResponsibles");
                });

            modelBuilder.Entity("WebApp.Models.OrganizationToSignatory", b =>
                {
                    b.Property<Guid>("OrganizationId");

                    b.Property<string>("ApplicationUserId");

                    b.HasKey("OrganizationId", "ApplicationUserId");

                    b.HasIndex("ApplicationUserId");

                    b.HasIndex("OrganizationId");

                    b.ToTable("OrganizationToSignatories");
                });

            modelBuilder.Entity("WebApp.Models.PurchaseProfile", b =>
                {
                    b.Property<Guid>("PurchaseProfileId")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("AuctionProfileId");

                    b.Property<string>("CompositionPrices");

                    b.Property<string>("DocumentsSupplied");

                    b.Property<double?>("LimitingOfDay");

                    b.Property<int>("NumberCopiesDocuments");

                    b.Property<double?>("OfficialExchangeRate");

                    b.Property<string>("PackaginGoods");

                    b.Property<string>("PaymentOrder");

                    b.Property<DateTime>("PeriodEndDateImplementation");

                    b.Property<double>("ProductCount");

                    b.Property<string>("ProductName");

                    b.Property<string>("ProductProperty");

                    b.Property<string>("PurchaseCurrency");

                    b.Property<string>("QualityAssurance");

                    b.Property<DateTime>("ShelfLifeWarehouse");

                    b.Property<DateTime?>("SupplyDate");

                    b.Property<string>("TypeOfVehicle");

                    b.Property<DateTime?>("UnloadingTime");

                    b.HasKey("PurchaseProfileId");

                    b.HasIndex("AuctionProfileId")
                        .IsUnique();

                    b.ToTable("PurchaseProfiles");
                });

            modelBuilder.Entity("WebApp.Models.Stock", b =>
                {
                    b.Property<Guid>("StockId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CheckinTerms");

                    b.Property<string>("Contacts");

                    b.Property<string>("LoadingAddress");

                    b.Property<string>("LoadingPlace");

                    b.Property<string>("NameOfResponsibility");

                    b.HasKey("StockId");

                    b.ToTable("Stocks");
                });

            modelBuilder.Entity("WebApp.Models.WaitingUser", b =>
                {
                    b.Property<string>("FromUserId");

                    b.Property<Guid>("OrganizationId");

                    b.Property<string>("InviteEmail");

                    b.Property<int>("Position");

                    b.HasKey("FromUserId", "OrganizationId", "InviteEmail");

                    b.HasIndex("FromUserId");

                    b.HasIndex("OrganizationId");

                    b.ToTable("WaitingUsers");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole")
                        .WithMany("Claims")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("WebApp.Models.ApplicationUser")
                        .WithMany("Claims")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("WebApp.Models.ApplicationUser")
                        .WithMany("Logins")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole")
                        .WithMany("Users")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("WebApp.Models.ApplicationUser")
                        .WithMany("Roles")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("WebApp.Models.AuctionProfile", b =>
                {
                    b.HasOne("WebApp.Models.Organization", "Organization")
                        .WithMany("AuctionProfiles")
                        .HasForeignKey("OrganizationId");
                });

            modelBuilder.Entity("WebApp.Models.Organization", b =>
                {
                    b.HasOne("WebApp.Models.OrganizationBankDetails", "OrganizationBankDetails")
                        .WithMany()
                        .HasForeignKey("OrganizationBankDetailsId");

                    b.HasOne("WebApp.Models.OrganizationProfile", "OrganizationProfile")
                        .WithMany()
                        .HasForeignKey("OrganizationProfileId");

                    b.HasOne("WebApp.Models.OrganizationProvisionRecipientBankDetails", "OrganizationProvisionRecipientBankDetails")
                        .WithMany()
                        .HasForeignKey("OrganizationProvisionRecipientBankDetailsId");

                    b.HasOne("WebApp.Models.Stock", "Stock")
                        .WithMany()
                        .HasForeignKey("StockId");
                });

            modelBuilder.Entity("WebApp.Models.OrganizationToAdmin", b =>
                {
                    b.HasOne("WebApp.Models.ApplicationUser", "ApplicationUser")
                        .WithMany("AdminFor")
                        .HasForeignKey("ApplicationUserId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("WebApp.Models.Organization", "Organization")
                        .WithMany("Holders")
                        .HasForeignKey("OrganizationId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("WebApp.Models.OrganizationToManager", b =>
                {
                    b.HasOne("WebApp.Models.ApplicationUser", "ApplicationUser")
                        .WithMany("ManagerFor")
                        .HasForeignKey("ApplicationUserId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("WebApp.Models.Organization", "Organization")
                        .WithMany("OrganizationToManagers")
                        .HasForeignKey("OrganizationId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("WebApp.Models.OrganizationToResponsible", b =>
                {
                    b.HasOne("WebApp.Models.ApplicationUser", "ApplicationUser")
                        .WithMany("ResponsibleFor")
                        .HasForeignKey("ApplicationUserId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("WebApp.Models.Organization", "Organization")
                        .WithMany("Responsibles")
                        .HasForeignKey("OrganizationId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("WebApp.Models.OrganizationToSignatory", b =>
                {
                    b.HasOne("WebApp.Models.ApplicationUser", "ApplicationUser")
                        .WithMany("SignatoryFor")
                        .HasForeignKey("ApplicationUserId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("WebApp.Models.Organization", "Organization")
                        .WithMany("Signatories")
                        .HasForeignKey("OrganizationId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("WebApp.Models.PurchaseProfile", b =>
                {
                    b.HasOne("WebApp.Models.AuctionProfile", "AuctionProfile")
                        .WithOne("PurchaseProfile")
                        .HasForeignKey("WebApp.Models.PurchaseProfile", "AuctionProfileId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("WebApp.Models.WaitingUser", b =>
                {
                    b.HasOne("WebApp.Models.ApplicationUser", "FromUser")
                        .WithMany()
                        .HasForeignKey("FromUserId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("WebApp.Models.Organization", "Organization")
                        .WithMany()
                        .HasForeignKey("OrganizationId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
