﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Google.Apis.Calendar.v3;

namespace WebApp.Code
{
    public static class Constant
    {
        //четкие критерии для картинок  
        public const string ImageTypes = @"image/gif,image/jpeg,image/png";
        public const long ImageMaxSize = 200000; //байт
        //regex pattent
        public const string RegexImageExt = "(.jpg|.png|.gif|.jpeg)";
        public const string UploadPath = "upload";
        //avatar path
        public const string AvatarPath = "avatar";


        //наш сайт
        public const string SiteUrl = "Http://www.test.ru";

        //roles
        public const string Admin = "Admin";
        public const string Manager = "Manager";
        public const string User = "User";
        //

        public static List<string> Roles = new List<string>()
        {
            Admin,
            Manager,
            User
        };


        public const string RegexCheckDoubleValue = @"^[ ]*\d{1,}[\.,]?\d{0,}[ ]*$";
        public const string RegexCheckIntValue = @"^[ ]*\d{1,}[ ]*$";


        //google
        public static string[] GoogleScopes = { CalendarService.Scope.Calendar };
        /// <summary>
        /// уникальная часть/чтобы ее обрезать 
        /// </summary>
        public const int EventIdUniquePart = 17;

    }

    public enum OrganizationPosition
    {
        Admin = 1,
        Manager = 2,
        Signatory = 3,
        Responsible = 4,
        //"админ", 
        //"контрактный управляющий" ,
        //"Ответственный за подписание",
        //"Ответственное должностное лицо"
    }


}
